'use strict';
// Import Express
const express = require('express');
// user router
const router = express.Router();
// Import body parser
const bodyParser = require('body-parser');

router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json({ limit: '50mb' }));
router.use(bodyParser.json());

// import  controllers
const userController = require('../src/user/userController');
const productController = require('../src/product/productController');
const orderController = require('../src/order/orderController');
const cartController = require('../src/cart/cartController');
const productHighlightController = require('../src/heighlightProduct/heighlightProductController');


// import validator Schemas
const userSchema = require('../src/user/userSchema');
const produtSchema = require('../src/product/productSchema');
const orderSchema = require('../src/order/orderSchema');
const cartSchema = require('../src/cart/cartSchema');

// import Validator class
const validator = require('../services/validator');

//user routes
router.route('/user/getAll')
    .get(userController.getAll);
router.route('/user/get/:id')
    .get(userController.getUser);
router.route('/user/update/:id')
    .patch(validator.validateBody(userSchema.updateUser), userController.updateUser);
router.route('/user/new')
    .post(validator.validateBody(userSchema.newUser), userController.newUser);
router.route('/user/new/admin')
    .post(validator.validateBody(userSchema.newUser), userController.newUserAdmin);
router.route('/user/login')
    .post(validator.validateBody(userSchema.login), userController.login);
router.route('/user/passowrd/reset/:username')
    .get(userController.resetPassword);
router.route('/user/delete/:id')
    .delete(userController.remove);
router.route('/user/order/products/:id')
    .post(userController.orderProducts);
router.route('/user/get/report')
    .post(userController.createReport);
router.route('/user/report/read')
    .get(userController.readReport);


//product routes
router.route('/product/new')
    .post(validator.validateBody(produtSchema.create), productController.create);
router.route('/product/getAll')
    .get(productController.getAll);
router.route('/product/get/:id')
    .get(productController.findOne);
router.route('/product/highlight/filter')
    .get(productController.getHighlightFilter);
router.route('/product/highlight/filter/getAll')
    .get(productController.getHighlightFilterAll);
router.route('/product/remove/:id')
    .delete(productController.remove);
router.route('/product/update/:id')
    .patch(validator.validateBody(produtSchema.update), productController.update);
router.route('/product/highlight/update/:id')
    .patch(validator.validateBody(produtSchema.updateHighlight), productHighlightController.update);
router.route('/product/highlight/remove/:id')
    .delete(productHighlightController.remove);

router.route('/product/get/report')
    .post(productController.createReport);

router.route('/images/read')
    .get(productController.readImage);
router.route('/report/read')
    .get(productController.readReport);
router.route('/images/remove')
    .post(validator.validateBody(produtSchema.imageRemove), productController.removeImage);

router.route('/order/new')
    .post(validator.validateBody(orderSchema.create), orderController.create);
router.route('/order/update/:id')
    .patch(validator.validateBody(orderSchema.update), orderController.update);
router.route('/orders/getAll')
    .get(orderController.getAllByUser);
router.route('/orders/getAll/admin')
    .get(orderController.getAll);
router.route('/order/delete/:id')
    .delete(orderController.remove);
router.route('/order/get/report')
    .post(orderController.createReport);
router.route('/order/report/read')
    .get(orderController.readReport);

router.route('/order/report/by/user')
    .post(orderController.createReportOrdersDoneByUsers);
router.route('/order/report/by/user/read')
    .get(orderController.readReportByUser);

router.route('/cart/new')
    .post(validator.validateBody(cartSchema.create), cartController.create);
router.route('/cart/get/:id')
    .get(cartController.findOne);
router.route('/cart/update/:id')
    .patch(validator.validateBody(cartSchema.update), cartController.update);
router.route('/cart/remove/:id')
    .delete(cartController.remove);

router.route('/testquary')
    .post(orderController.testQuary);
module.exports = router;
