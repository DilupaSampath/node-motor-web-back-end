const cors = require('cors');
const mongoose = require('mongoose');
const multer = require('multer');
const sharp = require('sharp')
fs = require('fs');
url = require('url');

mongoose.set('useNewUrlParser', true);
mongoose.set('useFindAndModify', false);
mongoose.set('useCreateIndex', true);
const bodyParser = require('body-parser');

const express = require('express');
const http = require('http');

const config = require('./config/config');
const routes = require('./routes/routes');

const server = express();
const server_port = config.web_port;

const session = require('express-session') // -> session storage using mongo
const path = require('path')

const DIR = './uploads';

let storage = multer.diskStorage({
    destination: (req, file, cb) => {
      cb(null, DIR);
    },
    filename: (req, file, cb) => {
      cb(null, file.fieldname + '-' + Date.now() + '.' + path.extname(file.originalname));
    }
});
let upload = multer({storage: storage});

server.use(session({ secret: 'zz', resave: true, saveUninitialized: true }))

server.use(bodyParser.json({limit: '50mb'}));
server.use(cors());
server.use(routes);
server.post('/api/upload',upload.single('photo'), function (req, res) {
    console.log(req.file);
    if (!req.file) {
        console.log("No file received");
        return res.send({
          success: false
        });
    
      } else {
      sharp('uploads/'+req.file.filename).resize({ height:100, width:100}).toFile('uploads/100_100/'+req.file.filename)
        .then(function(newFileInfo){
        console.log("Image Resized");
        console.log(newFileInfo);
        return res.send({
          success: true,
          file_path : config.api_url+ 'images/read?image=' + req.file.filename,
          resize_file_path : config.api_url+ 'images/read?image=' + req.file.filename+'&size=100_100',
        })
        })
        .catch(function(err){
          console.log(err);
        console.log("Got Error");
        return res.send({
          success: true,
          file_path : config.api_url+ 'images/read?image=' + req.file.filename+'&size=100_100',
          resize_file_path : false,
        })
        });
      }
});
// server.use(express.static(__dirname));
// server.use(express.static(path.join(__dirname, './dist/'))) // BUT ON PRODUCTION -> nginx
// Serve only the static files form the dist directory
server.use(express.static(path.join(__dirname, '/dist/')));

// For all GET requests, send back index.html (PathLocationStrategy)
server.get('*', (req,res) => {
    res.sendFile(path.join(__dirname, '/dist/index.html'));
});

server.use(express.json({limit: '50mb'}));
server.use(express.urlencoded({limit: '50mb'}));

const MongoClient = require('mongodb').MongoClient;

mongoose.connect(config.databaseUrl, { useNewUrlParser: true } ,function(err, client) {
    if(err) {
         console.log('Error occurred while connecting to MongoDB Atlas...\n',err);
    }
    console.log('Connected...');
 });
// mongoose.set('debug', true);
// Create socket server
var httpServer = http.createServer(server);
// http.createServer(function (req, res) {
//     //use the url to parse the requested url and get the image name
//     var query = url.parse(req.url,true).query;
//         pic = query.image;
 
//     //read the image using fs and send the image content back in the response
//     fs.readFile('uploads/' + pic, function (err, content) {
//         if (err) {
//             res.writeHead(400, {'Content-type':'text/html'})
//             console.log(err);
//             res.end("No such image");    
//         } else {
//             //specify the content type in the response will be an image
//             res.writeHead(200,{'Content-type':'image/jpg'});
//             res.end(content);
//         }
//     });
// });
// Start socket server
httpServer.listen(server_port, err => {
    if (err) {
        console.error(err);
    }
    else {
        console.log('server listening on port: ' + server_port);
    }
});

// var accountSid = 'AC7c775aec52eeca9219b451f137c08e78'; // Your Account SID from www.twilio.com/console
// var authToken = '20c326be306af76dc18ab004b12eb38f';   // Your Auth Token from www.twilio.com/console

// var twilio = require('twilio');
// var client = new twilio(accountSid, authToken);

// client.messages.create({
    // body: 'Hello from Node',
    // to: '+940712566198',  // Text this number
    // from: '+12565681959' // From a valid Twilio number
// })
// .then((message) => {console.log(message.sid);
// console.log('sadsad')});

module.exports = httpServer;