var insideCircle = require('geolocation-utils');
var moment = require('moment');
var request = require('request');
const geolib = require('geolib');
/* global google */
const trainService = require('../src/train/trainService');
const titmeTableService = require('../src/time-table/timeTableService');
const distance = require('google-distance-matrix');
distance.key('AIzaSyASr3NJ2EQUMz5tfrM7Fg_zgYY79m3yxE8');
var GOOGLE_NEAR_BY_PLACE_API_URL = 'https://maps.googleapis.com/maps/api/place/nearbysearch/json?';
var GOOGLE_NEAR_BY_PLACE_API_URL_TEXT_SEARCH = 'https://maps.googleapis.com/maps/api/place/textsearch/json?';
var GOOGLE_GET_DISTANCE_API_URL_TEXT_SEARCH = 'https://maps.googleapis.com/maps/api/distancematrix/json?';
var GOOGLE_DIRECTION_API_URL = 'https://maps.googleapis.com/maps/api/directions/json?';
//https://maps.googleapis.com/maps/api/directions/json?origin=6.937432,79.879112&destination=6.929831,79.861096&mode=transit&transit_mode=train&transit_routing_preference=less_walking&key=AIzaSyASr3NJ2EQUMz5tfrM7Fg_zgYY79m3yxE8

async function getDistance(data) {
    return new Promise(async (resolve, reject) => {
        console.log("***********8origin");
var origin={};
var destination ={};
makeNearByPlaceRequest({"lat":data.originLatAndLon.split(",")[0],"lang":data.originLatAndLon.split(",")[1]},async function (err, nearByPlaces) {
            if (!err){
                origin=nearByPlaces;
                // console.log(origin);
                makeNearByPlaceRequest({"lat":data.destinationsLatAndLon.split(",")[0],"lang":data.destinationsLatAndLon.split(",")[1]},async function (err, nearByPlaces) {
                    if (!err){
                        destination=nearByPlaces;
                   
// console.log(origin);
var exactOriginCordinates =await filterTrainStationsCordinates(origin);
var exactDestinationCordinates =await filterTrainStationsCordinates(destination);


if(exactOriginCordinates !=0 &&  exactDestinationCordinates !=0){

}else{
    return resolve("No direct train found");
}
        var origins = [exactOriginCordinates.cordinates.lat+","+exactOriginCordinates.cordinates.lng];
        var destinations = [exactDestinationCordinates.cordinates.lat+","+exactDestinationCordinates.cordinates.lng];
 
        var result;
        distance.transit_routing_preference('less_walking');
        distance.mode('transit');
        distance.transit_mode('rail');
        distance.matrix(origins, destinations, function (err, distances) {
           
            if (!err){
               
                result = distances;
                return resolve(result);
            }else{
 
                return reject(err) ;
            }
        });
                    }else{
                        destination={};
                    }
                        
                });
            }else{
                origin={};
            }
                
        } );



       
    });
    

}
async function getSpeed(data){
    var speed = await geolib.getSpeed(
       
        { latitude: data.firstLat, longitude: data.firstLng, time:data.firstTime },
        { latitude: data.secondLat, longitude: data.secondLng, time:data.secondTime  }
    );
    return speed;
}

//broadcast pump data
function filterRunningSchedules(trainData) {
var data = [];
data =trainData;
var result = data.filter(schedule => schedule.isRunung==true);
return result;
}
function filterTrainStationsCordinates(nearByLocationData) {
    return new Promise((resolve, reject) => {
    try {
    var data = [];
    data =nearByLocationData.results;
    var result = data.filter(item => item.types.includes("train_station")==true);
    if(result.length>0){
    
        return resolve({"name":result[0].name,"cordinates":result[0].geometry.location});
    }else{
        return resolve(0);
    }
   
    } catch (error) {
        return reject(1);
    }
});
    }

    function filterTrainStations(nearByLocationData) {
        return new Promise((resolve, reject) => {
        try {
        var data = [];
        data =nearByLocationData.results;
        var result = data.filter(item => item.types.includes("train_station")==true);
        
        if(result.length>0){
            var res=[];
            result.forEach(element => {
                res.push({
        "latitude": element.geometry.location.lat,
        "longitude":element.geometry.location.lng,
        "title":element.formatted_address,
        "draggable": true
      });
            });
            let center =     geolib.getCenter(res);
            return resolve({result:res,center:center});
        }else{
            return resolve(0);
        }
       
        } catch (error) {
            return reject(1);
        }
    });
}
async function  getDataWithArrivelTime (data) {
    // console.log(data);
    console.log("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");          

    // var ms = moment(new Date(1572772050315)).tz("Asia/Colombo").diff(moment(new Date(1572771467775)).tz("Asia/Colombo"));
        //             var d = moment.duration(ms);
        //   console.log(d.asMinutes());
        //   console.log(typeof(d));          
        // //             var s = d.format("HH:mm:ss");
        // //   console.log(s);
        //   console.log("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");          


    var speed = await geolib.getSpeed(
        { latitude: data.firstLat, longitude: data.firstLng, time:data.firstTime },
        { latitude: data.secondLat, longitude: data.secondLng, time:data.secondTime  }
    );
    
    // console.log(data.id);
    // var trainData =await trainService.findOne({name:'Test train XXX'});
    var timeTableData =await titmeTableService.findOne(data.id);
   
    var startTimeUser = moment(new Date(new Date(parseInt(data.firstTime)))).tz("Asia/Colombo").format("hh:mm a");
    var startTimeTrain = moment(new Date(new Date(parseInt(timeTableData.start_time)).getTime() - 10*60000)).tz("Asia/Colombo").format("hh:mm a");
    var currentstartTimeTrain = moment(new Date(new Date(parseInt(timeTableData.start_time)).getTime())).tz("Asia/Colombo").format("hh:mm a");
    var endTimeTrain = moment(new Date(new Date(parseInt(timeTableData.end_time)).getTime() )).tz("Asia/Colombo").format("hh:mm a");

    var defEndTimeTrain = moment(new Date(new Date(parseInt(timeTableData.end_time)).getTime())).tz("Asia/Colombo").format("hh:mm a");
    var beginningTime = moment(startTimeUser.toString(), 'h:mm a');
    var rs1 = beginningTime.isBetween(moment(currentstartTimeTrain.toString(), 'h:mm a'),moment(defEndTimeTrain.toString(), 'h:mm a'));
    var rs2 = beginningTime.isBetween(moment(startTimeTrain.toString(), 'h:mm a'),moment(currentstartTimeTrain.toString(), 'h:mm a'));
    var rs3 = beginningTime.isBetween(moment(startTimeTrain.toString(), 'h:mm a'),moment(endTimeTrain.toString(), 'h:mm a'));
    // console.log(rs1);
    // console.log(rs2);
    // console.log(beginningTime);
    console.log("*****************datew*******");
    console.log(moment(beginningTime));
    // console.log(startTimeTrain);
    console.log(moment(startTimeTrain.toString(), 'h:mm a'));
    // console.log(endTimeTrain);
    console.log(moment(endTimeTrain.toString(), 'h:mm a'));
    console.log(rs3);
    console.log("*****************datew*******");
        if(rs3){
    // moment(new Date(data.firstTime)).tz("Asia/Colombo").subtract(30, 'minutes');   
    var distan = geolib.getDistance(
    {latitude: data.secondLat, longitude: data.secondLng}, 
    { latitude: timeTableData.end.location.coordinates[1], longitude: timeTableData.end.location.coordinates[0]}
    );
    
    // var t22 =  await  makeDistanceRequest({lat1: timeTableData.end.location.coordinates[1], lng1: timeTableData.end.location.coordinates[0],lat2: data.secondLat, lng2: data.secondLng});
    // console.log("data ************************");
    // var ee = JSON.parse(t22);
    // console.log(ee);
    // console.log(ee.rows.elements);
    // console.log("data ************************");
   
var lastDis = geolib.convertDistance(distan, 'km');
var lastSpee = geolib.convertSpeed(speed, 'kmh');
if((rs2 ||rs1) && !(timeTableData.byUser)){
    console.log("in side by user");
    var t = await titmeTableService.findByIdAndUpdate(timeTableData._id,
        {
            byUser:true,
            act_start_time:moment(new Date().getTime()).tz("Asia/Colombo").valueOf(),
            act_end_time:moment(new Date (new Date(parseInt(data.secondTime)).setMinutes(new Date(parseInt(data.secondTime)).getMinutes() + Math.round((distan/speed)/60))).getTime()).tz("Asia/Colombo").valueOf(),
            short_end_time:moment(new Date (new Date(parseInt(data.secondTime)).setMinutes(new Date(parseInt(data.secondTime)).getMinutes() + Math.round((distan/speed)/60)))).tz("Asia/Colombo").format("HH:mm:ss")
        });
        
}else{
    var t = await titmeTableService.findByIdAndUpdate(timeTableData._id,
        {
            act_end_time:moment(new Date (new Date(parseInt(data.secondTime)).setMinutes(new Date(parseInt(data.secondTime)).getMinutes() + Math.round((distan/speed)/60))).getTime()).tz("Asia/Colombo").valueOf(),
            short_end_time:moment(new Date (new Date(parseInt(data.secondTime)).setMinutes(new Date(parseInt(data.secondTime)).getMinutes() + Math.round((distan/speed)/60)))).tz("Asia/Colombo").format("HH:mm:ss")
        });
        
}

// console.log("********************************** time");
//   console.log(moment(new Date (new Date(parseInt(data.secondTime)).setMinutes(new Date(parseInt(data.secondTime)).getMinutes() + Math.round((distan/speed)/60))).getTime()).tz("Asia/Colombo").valueOf());  
//   console.log("********************************** time");
 var arrvalTimeTemp = moment(new Date (new Date(parseInt(data.secondTime)).setMinutes(new Date(parseInt(data.secondTime)).getMinutes() + Math.round((distan/speed)/60)))).tz("Asia/Colombo");
//  console.log("arrvalTimeTemp");
//  console.log(arrvalTimeTemp);
//  console.log(moment(new Date(arrvalTimeTemp)).tz("Asia/Colombo"));
//  console.log(moment(new Date(arrvalTimeTemp)).tz("Asia/Colombo").valueOf());
//  console.log("arrvalTimeTemp");
 if(titmeTableService.chechDateIsPast(moment(new Date()).tz("Asia/Colombo").valueOf(),moment(new Date(arrvalTimeTemp)).tz("Asia/Colombo").valueOf())){

 } else{
    var res = {
        status:false,
        message:"Arrivel Time Is Not Valid For Given Period"
    };
 }
 var res = {
    status:true,
firstTime: moment(new Date(data.firstTime)).tz("Asia/Colombo").format("YYYY-MM-DD | HH:mm:ss"),
secondTime: moment(new Date(data.secondTime)).tz("Asia/Colombo").format("YYYY-MM-DD | HH:mm:ss"),
systemTime: moment(new Date()).tz("Asia/Colombo").format("YYYY-MM-DD | HH:mm:ss"),
trainName:timeTableData.train.name,
startStation:timeTableData.start,
endStation:timeTableData.end,
toDiurationInMinutes:((distan/speed)/60),
toDiurationInSec:((distan/speed)),
toDistanceInMeters:distan,
arrvalTime:moment(new Date (new Date(parseInt(data.secondTime)).setMinutes(new Date(parseInt(data.secondTime)).getMinutes() + Math.round((distan/speed)/60)))).tz("Asia/Colombo").format("YYYY-MM-DD | HH:mm:ss"),
speedMetersPerSecond:speed,
speedMetersPerMinute:speed*60
}

return res;
}else{
    var res = {
        status:false,
        message:"Please Select Valid Time Period"
    };
    return res;            
}
    }

 function getRealLocationData(data){
        return new Promise(async (resolve, reject) => {
    var responseObject={};
    var location ={};
                    // console.log(origin);
                    makeNearByPlaceRequest({"lat":data.lat,"lang":data.lang},async function (err, nearByPlaces) {
                        if (!err){
                            location=nearByPlaces;
    var exactLocationCordinates =await filterTrainStationsCordinates(location);
    responseObject.status=true;
    responseObject.data=exactLocationCordinates;
    return resolve(responseObject);
}else{
    responseObject.status=false;
    return  reject(responseObject)
    }
    });
        });
    }

   async function makeDistanceRequest(optionsDetails) {
    return new Promise(async (resolve, reject) => {
         var requestURL = GOOGLE_GET_DISTANCE_API_URL_TEXT_SEARCH + 'origins='+optionsDetails.lat1+','+optionsDetails.lng1+'&destinations='+optionsDetails.lat2+'%2C'+optionsDetails.lng2+'&transit_mode=train&mode=transit&key=AIzaSyASr3NJ2EQUMz5tfrM7Fg_zgYY79m3yxE8';
     console.log("requestURL");
     console.log(requestURL);
     console.log("requestURL");
         request(requestURL, function(err, response, data) {
           if (err || response.statusCode != 200) {
             return resolve(JSON.parse(JSON.stringify(data)));
           }else{
            return resolve(data);
           }
        //    var res =filterTrainStations(JSON.parse(data))
        //    callback(null, JSON.parse(data));
         })
       
    });
   }

function makeNearByPlaceRequest(optionsDetails, callback) {
   var options = {
        location: optionsDetails.lat+'%2C'+optionsDetails.lang,
        rankby: 'distance',
        type: 'train_station',
        key: 'AIzaSyASr3NJ2EQUMz5tfrM7Fg_zgYY79m3yxE8'
      }
    var requestURL = GOOGLE_NEAR_BY_PLACE_API_URL_TEXT_SEARCH + 'location='+optionsDetails.lat+'%2C'+optionsDetails.lang+'&rankby=distance&type=train_station&key=AIzaSyAfasOqXrX8t2kNPaTVcTJoXOPTVjkcnXY';

    request(requestURL, function(err, response, data) {
      if (err || response.statusCode != 200) {
        return callback(new Error('Google API request error: ' + data));
      }
      var res =filterTrainStations(JSON.parse(data))
      callback(null, JSON.parse(data));
    })
  }
 async function makeNearByPlaceRequestTrainFilter(optionsDetails, callback) {
    var options = {
         location: optionsDetails.lat+'%2C'+optionsDetails.lang,
         rankby: 'distance',
         type: 'train_station',
         key: 'AIzaSyASr3NJ2EQUMz5tfrM7Fg_zgYY79m3yxE8'
       }
     var requestURL = GOOGLE_NEAR_BY_PLACE_API_URL_TEXT_SEARCH + 'location='+optionsDetails.lat+'%2C'+optionsDetails.lang+'&rankby=distance&type=train_station&key=AIzaSyAfasOqXrX8t2kNPaTVcTJoXOPTVjkcnXY';
 
     request(requestURL,async function(err, response, data) {
       if (err || response.statusCode != 200) {
         return  callback(null,{"error":true,"message":"This Is Not Your Fault. Saver Was Unable To Complete Your Request...!!!"});
       }else{
           try {
            var res =await filterTrainStations(JSON.parse(data));
            callback(null,res);               
           } catch (error) {
            callback(null,{"error":true,"message":"This Is Not Your Fault. Saver Was Unable To Complete Your Request...!!!"});
           }
}
     })
   }
//broadcast pump data
// function getRealLocationData(data){
    function checkPointIsOnThePath (data)  {
    return new Promise(async (resolve, reject) => {
        try {
            // console.log(data.id);
            
            let item = await trainService.findOne(data.id);
            // console.log(item);
            for (var i = 0; i < item.selectedPoints.length; i++) {
                // console.log(data[i]);
                var o = JSON.stringify(item.selectedPoints[i]);                
                var g = JSON.parse(o);
            var isInPath = insideCircle.insideCircle(data.point, {lat: g.coordinates[1], lon: g.coordinates[0]}, 30);
                if (isInPath) {
                    return  resolve(true);
                }
              }
              return resolve(false);
        } catch (error) {
            return reject(error);
        }
    });
    
}



// function  checkPointIsOnThePath (point,data) {

//     for (var i = 0; i < data.path_cordinates.length; i++) {
//         console.log(data.path_cordinates); // result: "My","name"
//       var isInPath =   geolib.isPointWithinRadius(
//             point,
//             data.path_cordinates[i],
//             500
//         );
//         console.log(isInPath); // result: "My","name"

//         if (isInPath) {
//             return true;
//         }
//       }
//       return false;
//     }
module.exports = {
    getDistance,
    getDataWithArrivelTime,
    makeNearByPlaceRequest,
    getRealLocationData,
    checkPointIsOnThePath,
    makeNearByPlaceRequestTrainFilter,
    getSpeed
};