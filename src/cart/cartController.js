const cartService = require('./cartService');
const response = require('../../services/responseService');

/**
 * get all 
 */
module.exports.getAll = async (req, res) => {
    try {
        let items = await cartService.getAll({})
        response.successWithData(items, res)
    } catch (error) {
        response.customError('' + error, res)
    }
}


/**
 * get one item
 */
module.exports.findOne = async (req, res) => {
    try {
        console.log(req.params.id);
        let item = await cartService.findOne(req.params.id)
        response.successWithData(item, res)
    } catch (error) {
        response.customError('' + error, res)
    }
}

/**
 * create item
 */
module.exports.create = async (req, res) => {
    try {
        let item = await cartService.create(req.body);
        response.successWithData(item, res)
    } catch (error) {
        response.customError('' + error, res)
    }
}

/**
 * update item by id
 * @param {*} req
 * @param {*} res
 */
module.exports.update = async (req, res) => {
    try {
        console.log(req.params.id);
        console.log(req.body);
        console.log("=========");
        let details = JSON.parse(JSON.stringify(req.body));
        // delete details.id;
        let item = await cartService
            .findByIdAndUpdate(req.params.id, details);
        if (item == null || item == undefined)
            response.customError("Invalid id", res)
        else
            response.successWithData(item, res);
    } catch (error) {
        response.customError('' + error, res);
    }
};

/**
 * remove item by id
 * @param {*} req
 * @param {*} res
 */
module.exports.remove = async (req, res) => {
    try {
        let item = await cartService.delete(req.body.id);
        response.successWithData(item, res);
    } catch (error) {
        response.customError('' + error, res);
    }
};



/**
 * remove item by id
 * @param {*} req
 * @param {*} res
 */
module.exports.validate = async (req, res) => {
    try {
        let item = await cartService.validate(req.body);
        response.successWithData(item, res);
    } catch (error) {
        response.customError('' + error, res);
    }
};