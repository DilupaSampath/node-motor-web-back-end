const cartModel = require('./cartModel');
const userModel = require('../user/userModel');
var nodemailer = require('nodemailer');
var smtpTransport = require('nodemailer-smtp-transport');
var transporter = nodemailer.createTransport(smtpTransport({
    service: 'gmail',
    auth: {
        user: 'madsampath94@gmail.com',
        pass: 'dsmax071'
    }
}));

var paymentMethods = {
    COD: { img: 'https://pngimage.net/wp-content/uploads/2018/05/cash-on-delivery-png-5.png', txt: "Cash on delivery" },
    CC: { img: 'http://dominionvoice.com/wp-content/uploads/2013/08/Fully-Paid-300x265.jpg', txt: "Credit Card Payment" },
    DC: { img: 'http://dominionvoice.com/wp-content/uploads/2013/08/Fully-Paid-300x265.jpg', txt: "Debit Card Payment" },
}


/**
 * find from the system
 */
module.exports.find = (condition) => {
    return new Promise((resolve, reject) => {
        cartModel.find(condition).exec((err, cart) => {

            err ? reject(err) : resolve(cart);
        });
    })
};

module.exports.getAll = (condition) => {
    return new Promise((resolve, reject) => {
        cartModel.find(condition).exec((err, cart) => {
            err ? reject(err) : resolve(cart);
        });
    })
};
/**
 * remove  from the system
 */
module.exports.delete = (id) => {
    return new Promise((resolve, reject) => {
        cartModel.findByIdAndDelete(id,
            async (err, cart) => {
                if (err)
                    reject(err);
                else if (cart != null || cart != undefined) {
                    resolve("successfully deleted");
                } else
                    reject("Invalid id");
            });
    })
}
/**
 * register new to the system
 */
module.exports.create = (body) => {
    return new Promise(async (resolve, reject) => {
        try {
            console.log(body.cart_items);
            const cart = new cartModel();

            cart.cart_items = JSON.parse(JSON.stringify(body.cart_items));
            console.log(cart);
            cart.save(async (errorCart, cartData) => {
                if (errorCart) {
                    reject(errorCart);
                } else {
                    resolve(cartData);
                }
            });
        } catch (errorSave) {
            reject({ error: true, msg: errorSave });
        }
    });
}

/**
 * find  from the system by id
 */
module.exports.findOne = (id) => {
    return new Promise((resolve, reject) => {
        cartModel.findById(id)
            .exec((err, data) => {
                err ? reject(err) : resolve(data);
            });
    })
};
/**
 * find and update from the system by id
 */
module.exports.findByIdAndUpdate = async (id, updateObject) => {
    return new Promise(async (resolve, reject) => {
        try {
            console.log(id);
            console.log(updateObject);
            cartModel.findById(id , function (err1, cartData) {
            if (err1) {
                reject(err1);
            }
            else if (cartData != undefined || cartData != null) {
                cartModel.findByIdAndUpdate(id, {$set: updateObject}).exec((err, data) => {
                    err ? reject(err) : resolve(data);
                });
            }else{
                reject("Cart ID not found");
            }

        });
        } catch (error) {
            reject(error);
        }
    })
};


function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
}
