const joi = require('joi');

// updateValve registration Schema and validations to be done
module.exports.create = joi.object().keys({

  cart_items: joi.array().items({
    product: joi.object().required(),
    amount:joi.number().required(),
  }).required()
});


module.exports.update = joi.object().keys({

  cart_items: joi.array().items({
    product: joi.object(),
    amount:joi.number(),
  }).required()
});