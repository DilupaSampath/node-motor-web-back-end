const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const cartSchema = new Schema(
    {
        cart_items: [{
            product: { type: Object },
            amount: { type: Number }
        }]
    }

    , { timestamps: true }
);
// cartSchema.index({ cart: "2dsphere" });
module.exports = mongoose.model('carts', cartSchema);