const heighlightService = require('./heighlightProductService');
const response = require('../../services/responseService');

/**
 * get all 
 */
module.exports.getAll = async (req, res) => {
    try {
        console.log(req.query);
        let items = await heighlightService.getAll(req.query);
        response.successWithData(items, res)
    } catch (error) {
        response.customError('' + error, res)
    }
}


/**
 * get one item
 */
module.exports.findOne = async (req, res) => {
    try {
        let item = await heighlightService.findOne(req.body.id)
        response.successWithData(item, res)
    } catch (error) {
        response.customError('' + error, res)
    }
}

/**
 * create item
 */
module.exports.create = async (req, res) => {
    try {
        let item = await heighlightService.create(req.body);
        response.successWithData(item, res)
    } catch (error) {
        response.customError('' + error, res)
    }
}
/**
 * remove item by id
 * @param {*} req
 * @param {*} res
 */
module.exports.remove = async (req, res) => {
    try {
        console.log(req.params.id);
        let item = await heighlightService.delete(req.params.id);
        response.successWithData(item, res);
    } catch (error) {
        response.customError('' + error, res);
    }
};

/**
 * update item by id
 * @param {*} req
 * @param {*} res
 */
module.exports.update = async (req, res) => {
    try {
        let details = JSON.parse(JSON.stringify(req.body));
        delete details.id;
        delete details._id;
        delete details.createdAt;
        delete details.updatedAt;
        delete details.__v;
        console.log(req.params.id);
        console.log('req.params.id');
        let item = await heighlightService
            .findByIdAndUpdate(parseInt(req.params.id), details);
        if (item == null || item == undefined)
            response.customError("Invalid id", res)
        else
            response.successWithData(item, res);
    } catch (error) {
        response.customError('' + error, res);
    }
};



/**
 * remove item by id
 * @param {*} req
 * @param {*} res
 */
module.exports.validate = async (req, res) => {
    try {
        let item = await productService.validate(req.body);
        response.successWithData(item, res);
    } catch (error) {
        response.customError('' + error, res);
    }
};