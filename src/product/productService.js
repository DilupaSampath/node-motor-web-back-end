const productModel = require('./productModel');
const heighlightService = require('../heighlightProduct/heighlightProductService');
const heighlightProductModel = require('../heighlightProduct/heighlightProductModel');
var fs = require('fs');
var pdf = require('html-pdf');
var options = { format: 'Letter' };
var moment = require('moment-timezone');
/**
 * create product report
 */
module.exports.createReport = (start, end) => {
    return new Promise((resolve, reject) => {
        console.log('createReport');
        console.log(moment(new Date(new Date(start))).tz("Asia/Colombo").toISOString());
        productModel.find({
            createdAt: {
                $gte: moment(new Date(new Date(start))).tz("Asia/Colombo").toISOString(),
                $lte: moment(new Date(new Date(end))).tz("Asia/Colombo").toISOString()
            }
        }).exec((err, product) => {
            // err ? reject(err) : resolve(product);

            if (err) {
                reject(err);
            } else {
                if (product) {
                    // add events (here, we draw headers on each new page)

                    // cancel event so the automatic page add is not triggered
                    var dataPdf = findProductsFroPdf(product);
                    // ev.cancel = true;
                    var htmlBody = createMailOptionsFroStockCountTemplate(dataPdf,start,end);
                    pdf.create(htmlBody, options).toFile('report_product.pdf', function (err, res) {
                        if (err) {
                            return reject(err)
                        }
                        else {
                            resolve(res);
                        }
                        // console.log(res); // { filename: '/app/businesscard.pdf' }

                    });
                    //  resolve(pdf);

                    // pdf.addPage();

                    // draw content, by passing data to the addBody method
                }
            }
        });
    });
};
function findProductsFroPdf(list) {
    var productList = [];
    list.forEach(element => {
        productList.push({
            id: element.id + '',
            name: element.name + '',
            date: element.date + '',
            actual_price: element.actual_price + '',
            original_price: element.original_price + '',
            updatedAt: element.updatedAt + '',
            stock_count: (element.stock_count ? element.stock_count : 0) + ''
        });
    });
    console.log('productList pdf');
    console.log(productList);
    return productList;
};
function createMailOptionsFroStockCountTR(trs) {
    var trList = '';

    trs.forEach(element => {
        trList += `<tr>
      <td>`+ element.id + `</td>
      <td>`+ element.name + `</td>
      <td>`+ element.date + `</td>
      <td>`+ element.actual_price + `</td>
      <td>`+ element.updatedAt.split('GMT')[0] + `</td>
      <td>`+ element.stock_count + `</td>
    </tr>`;
    });
    console.log('trs&&&&&&&&&&&&&&');
    console.log(trList);
    console.log('trs&&&&&&&&&&&&&&');
    return trList;
}
function createMailOptionsFroStockCountTemplate(array,start,end) {
    console.log('TRS-create ***********');
    console.log(array);
    console.log('TRS-create ***********');
    return `
   <!DOCTYPE html>
<html>
<head>
<style>
table {
  font-family: arial, sans-serif;
  font-size: small;
  border-collapse: collapse;
  width: 90%;
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}

</style>
</head>
<body>

<h3>Stock report for `+start+` to `+end+`</h3>

<table>
<tr>
    <th>Product ID</th>
    <th>Product</th>
    <th>Date</th>
    <th>Actual Price</th>

    <th>Last Update</th>
    <th>Stock</th>
  </tr>
  `+ createMailOptionsFroStockCountTR(array) + `
</table>

</body>
</html>

   `
}
/**
 * find from the system
 */
module.exports.findProducts = (condition) => {
    return new Promise((resolve, reject) => {
        productModel.find(condition).exec((err, product) => {
            err ? reject(err) : resolve(product);
        });
    })
};

function findProductsLocal(condition) {
    return new Promise((resolve, reject) => {
        productModel.findOne(condition).exec((err, product) => {
            err ? reject(err) : resolve(product);
        });
    })
};
module.exports.getAll = (condition) => {
    return new Promise((resolve, reject) => {
        productModel.find(condition).exec((err, product) => {
            err ? reject(err) : resolve(product);
        });
    })
};
module.exports.getAllHighLight = async (condition) => {
    return new Promise(async (resolve, reject) => {
        var resProducts = [];
        heighlightProductModel.find(condition).exec(async (err, products) => {
            try {
                if (!err) {
                    console.log('heighlightProductModel products **');
                    console.log(products);
                    resolve(products);
                } else {
                    //err
                    reject(err)
                }
            } catch (error) {
                reject(error)
            }
            // err ? reject(err) : resolve(product);

        });
    })
};
module.exports.getHighLight = async (condition) => {
    return new Promise(async (resolve, reject) => {
        var resProducts = [];
        heighlightProductModel.find(condition).exec(async (err, products) => {
            try {
                if (!err) {
                    if (products.length > 0) {
                        console.log('heighlightProductModel products **');
                        console.log(products);
                        products.forEach(async (highLightElement, index) => {
                            let mainProduct = await findProductsLocal({ id: highLightElement.id });
                            if (mainProduct) {
                                resProducts.push(mainProduct);
                            }

                            if (index >= products.length - 1) {
                                console.log('heighlightProductModel res products **');
                                console.log(resProducts);
                                resolve(resProducts);
                            }
                        });

                    } else {
                        // noroducts
                        resolve(resProducts);
                    }
                } else {
                    //err
                    reject(err)
                }
            } catch (error) {
                reject(error)
            }
            // err ? reject(err) : resolve(product);

        });
    })
};
/**
 * remove  from the system
 */
module.exports.delete = (id) => {
    return new Promise((resolve, reject) => {
        productModel.findOneAndDelete({ id: id },
            async (err, product) => {
                if (err)
                    reject(err);
                else if (product != null || product != undefined) {
                    resolve("successfully deleted");
                } else
                    reject("Invalid id");
            });
    })
}
/**
 * register new to the system
 */
module.exports.create = async (body) => {
    return new Promise((resolve, reject) => {
        productModel.findOne({ id: body.id }, async function (err, productData) {
            console.log(productData);
            console.log(err);
            if (err) {
                reject(err);
            }
            else if (productData == undefined || productData == null) {
                const product = new productModel();
                product.id = body.id;
                product.name = body.name;
                product.date = body.date;
                product.reduction = body.reduction;
                product.sale = body.sale;
                product.currentRating = body.currentRating;
                product.categories = body.categories;
                product.description = body.description;
                product.image_urls = body.image_urls;
                product.actual_price = body.actual_price;
                product.original_price = body.original_price;
                product.stock_count = body.stock_count;
                product.save(async (error, productCb) => {
                    if (error) {
                        reject(error);
                    } else {
                        const heighlightProduct = new heighlightProductModel();
                        heighlightProduct.id = product.id;
                        heighlightProduct['is_new_arrivals'] = false;
                        heighlightProduct['is_on_sale'] = false;
                        heighlightProduct['is_best_rated'] = false;
                        heighlightProduct['is_featured'] = false;
                        try {
                            console.log('in highlight');
                            let items = await heighlightService.create(heighlightProduct);
                            resolve(productCb);
                        } catch (error1) {
                            console.log('in highlight original delete');
                            productModel.findOneAndDelete({ id: heighlightProduct },
                                async (err, product1) => {
                                    if (err)
                                        reject(err);
                                    else if (product1 != null || product1 != undefined) {
                                        console.log('in highlight original deletedddddd');
                                        resolve("cannot create highlight and original product was successfully deleted");
                                    } else
                                        reject("cannot create highlight and original product ID is Invalid");
                                });
                            reject(error1);
                        }


                    }

                });
            } else {
                reject("Product ID already exists");
            }
        })
    })
}




/**
 * find  from the system by id
 */
module.exports.findOne = (id) => {
    return new Promise((resolve, reject) => {
        productModel.findOne({ id: id })
            .exec((err, data) => {
                err ? reject(err) : resolve(data);
            });
    })
};


/**
 * find and update from the system by id
 */
module.exports.findByIdAndUpdate = async (id, updateObject) => {
    return new Promise(async (resolve, reject) => {
        try {
            productModel.findOne({ id: id }, function (err1, productData) {
                console.log(id);
                console.log(updateObject);
                if (err1) {
                    reject(err1);
                }
                else if (productData != undefined || productData != null) {
                    productModel.update({ id: id }, { $set: updateObject }).exec((err, data) => {
                        err ? reject(err) : resolve(data);
                    });
                } else {
                    reject("Product ID not found");
                }

            });
        } catch (error) {
            reject("Error while updating product");
        }
    })
};

/**
 * find  from the system by id
 */
module.exports.validate = async (data) => {
    return new Promise(async (resolve, reject) => {
        try {
            let result = await trainService.findTrainIdAccordingToLocation(data);
            resolve(result);
        } catch (error) {
            reject(error);
        }
    })
};
