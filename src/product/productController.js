const productService = require('./productService');
const response = require('../../services/responseService');
const config = require('../../config/config');
fs = require('fs');
url = require('url');
/**
 * get all 
 */
module.exports.getAll = async (req, res) => {
    try {
        console.log(req.query);
        let items = await productService.getAll(req.query);
        response.successWithData(items, res)
    } catch (error) {
        response.customError('' + error, res)
    }
}
module.exports.getHighlightFilter = async (req, res) => {
    try {
        console.log(req.query);
        let items = await productService.getHighLight(req.query);
        response.successWithData(items, res)
    } catch (error) {
        response.customError('' + error, res)
    }
}
module.exports.getHighlightFilterAll = async (req, res) => {
    try {
        console.log(req.query);
        let items = await productService.getAllHighLight({});
        response.successWithData(items, res)
    } catch (error) {
        response.customError('' + error, res)
    }
}
/**
 * get one item
 */
module.exports.findOne = async (req, res) => {
    try {
        console.log(req.params.id);
        let item = await productService.findOne(req.params.id)
        response.successWithData(item, res)
    } catch (error) {
        response.customError('' + error, res)
    }
}
/**
 * get report
 */
module.exports.createReport = async (req, res) => {
    try {
        console.log('createReport');
        console.log(req.body);
        let pdf = await productService.createReport(req.body.start,req.body.end);
        // pdf.pipe(res);
        // pdf.end();
        response.successWithData(pdf, res)
    } catch (error) {
        response.customError('' + error, res)
    }
}
/**
 * file upload
 */
module.exports.findOne = async (req, res) => {
    try {
        console.log(req.params.id);
        let item = await productService.findOne(req.params.id)
        response.successWithData(item, res)
    } catch (error) {
        response.customError('' + error, res)
    }
}
/**
 * create item
 */
module.exports.create = async (req, res) => {
    try {
        let item = await productService.create(req.body);
        response.successWithData(item, res)
    } catch (error) {
        response.customError('' + error, res)
    }
}
module.exports.removeImage = async (req, res) => {
    try {
        var item = req.body;
        console.log('************************************');
        console.log('./uploads/' + item.image_name);
        console.log('************************************');
        //read the image using fs and send the image content back in the response
        fs.unlink('./uploads/' + item.image_name, function (err) {
            if (err) {
                response.customError('' + err, res)
            } else {
                console.log('File deleted!');
                response.successWithData('File deleted!', res)
            }
            // if no error, file has been deleted successfully
        });
    } catch (error) {
        // res.end(error);
        response.customError('' + error, res)
    }
}
module.exports.readImage = async (req, res) => {
    try {
        var query = url.parse(req.url, true).query;
        var path = '';
        pic = query.image;
        size = query.size;

        //read the image using fs and send the image content back in the response

        if (size) {
            path = size + '/' + pic;
        } else {
            path = pic;
        }
        fs.readFile('uploads/' + path, function (err, content) {
            if (err) {
                res.writeHead(400, { 'Content-type': 'text/html' })
                console.log(err);
                res.end("No such image");
            } else {
                //specify the content type in the response will be an image
                res.writeHead(200, { 'Content-type': 'image/png' });
                res.end(content);
            }
        });
    } catch (error) {
        response.customError('' + error, res)
    }
}
module.exports.readReport = async (req, res) => {
    try {

        //read the image using fs and send the image content back in the response

        fs.readFile('report_product.pdf', function (err, content) {
            if (err) {
                res.writeHead(400, { 'Content-type': 'application/pdf' })
                console.log(err);
                res.end("No such pdf");
            } else {
                //specify the content type in the response will be an image
                res.writeHead(200, { 'Content-type': 'application/pdf' });
                res.end(content);
            }
        });
    } catch (error) {
        response.customError('' + error, res)
    }
}
//     var query = url.parse(req.url,true).query;
//         pic = query.image;

//     //read the image using fs and send the image content back in the response
//     fs.readFile('uploads/' + pic, function (err, content) {
//         if (err) {
//             res.writeHead(400, {'Content-type':'text/html'})
//             console.log(err);
//             res.end("No such image");    
//         } else {
//             //specify the content type in the response will be an image
//             res.writeHead(200,{'Content-type':'image/jpg'});
//             res.end(content);
//         }
//     });
/**
 * update item by id
 * @param {*} req
 * @param {*} res
 */
module.exports.update = async (req, res) => {
    try {
        let details = JSON.parse(JSON.stringify(req.body));
        delete details.id;
        delete details._id;
        delete details.createdAt;
        delete details.updatedAt;
        delete details.__v;
        console.log(req.params.id);
        let item = await productService
            .findByIdAndUpdate(parseInt(req.params.id), details);
        if (item == null || item == undefined)
            response.customError("Invalid id", res)
        else
            response.successWithData(item, res);
    } catch (error) {
        response.customError('' + error, res);
    }
};

/**
 * remove item by id
 * @param {*} req
 * @param {*} res
 */
module.exports.remove = async (req, res) => {
    try {
        console.log(req.params.id);
        let item = await productService.delete(req.params.id);
        response.successWithData(item, res);
    } catch (error) {
        response.customError('' + error, res);
    }
};



/**
 * remove item by id
 * @param {*} req
 * @param {*} res
 */
module.exports.validate = async (req, res) => {
    try {
        let item = await productService.validate(req.body);
        response.successWithData(item, res);
    } catch (error) {
        response.customError('' + error, res);
    }
};