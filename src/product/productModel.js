const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const productSchema = new Schema(
    {
        id: {
            type: Number,
            required: true,
            unique: true
        },
        name: {
            type: String,
            required: true
        },
        date :  { type: String },
        categories: {},
        description: { type: String },
        image_urls: [],
        actual_price : { type: Number },
        stock_count : { type: Number,
            default: 0 },
        original_price : { type: Number },

    }, { timestamps: true }
);
// productSchema.index({ product: "2dsphere" });
module.exports = mongoose.model('products', productSchema);
