const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const orderSchema = new Schema(
    {
        products: [],
        order_id :{ type: String },
        total_price:{ type: Number },
        payment_type: { type: String },
        order_address: { type: String },
        recipient_name: { type: String },
        contact_number: { type: String },
        delivery_cost: { type: Number },
        delivery_date: { type: String },
        payment_status: { type: String },
        status: { type: String },
        created_on_date: { type: Number },
        user_id: { type: Schema.Types.ObjectId,
            ref: "User"
         },

    }, { timestamps: true }
);
// orderSchema.index({ order: "2dsphere" });
module.exports = mongoose.model('orders', orderSchema);