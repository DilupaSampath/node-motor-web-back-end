const orderModel = require('./orderModel');
const userModel = require('../user/userModel');
const productModel = require('../product/productModel');
const userService = require('../user/userService');
var nodemailer = require('nodemailer');
var smtpTransport = require('nodemailer-smtp-transport');
var pdf = require('html-pdf');
var options = { format: 'A3', orientation: "landscape" };
var moment = require('moment-timezone');
const momentFormat = require('moment');
var transporter = nodemailer.createTransport(smtpTransport({
    service: 'gmail',
    auth: {
        user: 'madsampath94@gmail.com',
        pass: 'dsmax071'
    }
}));
module.exports.createReport = (start, end, status = false) => {
    return new Promise((resolve, reject) => {
        var quary = {};
        if (status && status !== 'ALL') {
            quary = {
                $and: [{
                    createdAt: {
                        $gte: moment(new Date(new Date(start))).tz("Asia/Colombo").toISOString(),
                        $lte: moment(new Date(new Date(end))).tz("Asia/Colombo").toISOString()
                    }
                },
                {
                    payment_status: status
                }
                ]
            };
        } else {
            quary = {
                createdAt: {
                    $gte: moment(new Date(new Date(start))).tz("Asia/Colombo").toISOString(),
                    $lte: moment(new Date(new Date(end))).tz("Asia/Colombo").toISOString()
                }
            };
        }
        console.log(quary);
        console.log("quary****************");
        console.log(moment(new Date(new Date(start))).tz("Asia/Colombo").toISOString());
        orderModel.find(quary).exec(async (err, product) => {
            // err ? reject(err) : resolve(product);

            if (err) {
                reject(err);
            } else {
                if (product) {

                    // add events (here, we draw headers on each new page)
                    var totalQuary = {
                        $match: quary
                    }
                    console.log(JSON.stringify(totalQuary));
                    var totalDetails = await getAllTotal(product);
                    console.log("***********totalDetails");
                    console.log(totalDetails);
                    console.log("***********totalDetails");
                    // cancel event so the automatic page add is not triggered
                    var dataPdf = findProductsFroPdf(product);
                    // ev.cancel = true;
                    var htmlBody = createFullPdfTemplate(dataPdf, start, end, status, totalDetails);
                    pdf.create(htmlBody, options).toFile('report_order.pdf', function (err, res) {
                        if (err) {
                            return reject(err)
                        }
                        else {
                            resolve(res);
                        }
                        // console.log(res); // { filename: '/app/businesscard.pdf' }

                    });
                    //  resolve(pdf);

                    // pdf.addPage();

                    // draw content, by passing data to the addBody method
                }
            }
        });
    });
};
function findProductsFroPdf(list) {
    var productList = [];
    list.forEach(element => {
        var products = `<ul>`;

        element.products.forEach(elementIn => {

            products += `  <li>` + (elementIn.id ? elementIn.id : '------') + '<B>|</B>' + (elementIn.qty) + `</li>`

        });
        productList.push({
            id: element.order_id + '',
            total_price: element.total_price + '',
            payment_type: element.payment_type + '',
            contact_number: element.contact_number + '',
            delivery_cost: element.delivery_cost + '',
            status: element.status + '',
            payment_status: element.payment_status + '',
            date: element.createdAt,
            products: products + '</ul>'
        });
    });
    console.log('productList pdf');
    console.log(productList);
    return productList;
};
function createFullPdfTemplate(array, start, end, status, total) {
    console.log('TRS-create ***********');
    console.log(array);
    console.log('TRS-create ***********');
    var statusTemp = '';
    if (status) {
        statusTemp = `<h3>For Order Status :<B>` + status + `</B></h3>`;
    } else {
        statusTemp = `<h3>For Order Status :<B>All</B></h3>`;
    }
    var total = total && total !== undefined && total !== 'undefined' && total !== null && total !== 'null' ? parseFloat(total+"").toFixed(4) : 0.00
    return `
   <!DOCTYPE html>
<html>
<head>
<style>
table {
  font-family: arial, sans-serif;
  font-size: small;
  border-collapse: collapse;
  width: 90%;
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}

</style>
</head>
<body>

<h3>Order report for `+ start + ` to ` + end + `</h3><br>
`+ statusTemp + `<br>
<h3>Total Income :<B>` + parseFloat(total+"").toFixed(3) + `</B></h3>

<table>
<tr>
    <th>Order ID</th>
    <th>Total Price</th>
    <th>Payment Type</th>
    <th>Contact Number</th>
    <th>Delivery Cost</th>
    <th>Payment Status</th>
    <th>Date</th>
    <th>products</th>
  </tr>
  `+ createTrs(array) + `
</table>

</body>
</html>

   `
}
function createTrs(trs) {
    var trList = '';

    trs.forEach(element => {
        console.log(new Date(element.date));
        var eleDate = moment(new Date(element.date)).tz("Asia/Colombo");
        eleDate = momentFormat(eleDate).format('MM/DD/YYYY');
        trList += `<tr>
      <td>`+ element.id + `</td>
      <td>`+ element.total_price + `</td>
      <td>`+ element.payment_type + `</td>
      <td>`+ element.contact_number + `</td>
      <td>`+ element.delivery_cost + `</td>
      <td>`+ element.payment_status + `</td>
      <td>`+ eleDate + `</td>
      <td>`+ element.products + `</td>
    </tr>`;
    });
    return trList;
}
function createTrsForUserReport(trs) {
    var trList = '';

    trs.forEach((element, index) => {
        var name = '';
        var email = '';
        var role = '';
        var contact_number = '';
        console.log('element in TR %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');
        console.log(element);
        console.log('element in TR %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');
        if (element.userData) {
            name = element.userData.first_name ? element.userData.first_name : '--' + ' ' + element.userData.last_name ? element.userData.last_name : '--';
            email = element.userData.email ? element.userData.email : '-----';
            role = element.userData.role ? element.userData.role : '-----';
            contact_number = element.userData.contact_number ? element.userData.contact_number : '-----';
        } else {
            name = "-----";
            email = "-----"
            role = "-----"
            contact_number = "-----"
        }
        if (index === 0) {
            trList += `<tr style="background-color: rgb(208, 250, 166);">`;
        } else {
            trList += `<tr>`;
        }

        trList += `<td>` + element._id + `</td>
      <td>`+ name + `</td>
      <td>`+ email + `</td>
      <td>`+ role + `</td>
      <td>`+ element.maxQuantity + `</td>
      <td>`+ element.maoOrders + `</td>
    </tr>`;
    });
    return trList;
}
function createFullPdfTemplateForUserReport(array, start, end) {
    console.log('TRS-create ***********');
    console.log(array);
    console.log('TRS-create ***********');

    return `
   <!DOCTYPE html>
<html>
<head>
<style>
table {
  font-family: arial, sans-serif;
  font-size: small;
  border-collapse: collapse;
  width: 90%;
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}

</style>
</head>
<body>

<h3>All orders done by users `+ start + ` to ` + end + `</h3><br>
<table>
<tr>
    <th>User ID</th>
    <th>Name</th>
    <th>Email</th>
    <th>Role</th>
    <th>Total</th>
    <th>Order count</th>
  </tr>
  `+ createTrsForUserReport(array) + `
</table>

</body>
</html>

   `
}
var paymentMethods = {
    COD: { img: 'https://pngimage.net/wp-content/uploads/2018/05/cash-on-delivery-png-5.png', txt: "Cash on delivery" },
    CC: { img: 'http://dominionvoice.com/wp-content/uploads/2013/08/Fully-Paid-300x265.jpg', txt: "Credit Card Payment" },
    DC: { img: 'http://dominionvoice.com/wp-content/uploads/2013/08/Fully-Paid-300x265.jpg', txt: "Debit Card Payment" },
}

function createMailOptionsFroStockCount(user, list) {
    console.log('list-create ***********');
    console.log(list);
    console.log('list-create ***********');
    htmlTemp = createMailOptionsFroStockCountTemplate(list);
    subject = `Motor Web Stock Alert :`;

    var mailOptions = {
        from: 'madsampath94@gmail.com',//replace with your email
        to: user.email,//replace with your email
        subject: subject,
        html: htmlTemp
    };
    return mailOptions;
}
function createMailOptionsFroStockCountTR(trs) {
    var trList = '';
    console.log('trs&&&&&&&&&&&&&&');
    console.log(trs);
    console.log('trs&&&&&&&&&&&&&&');
    trs.forEach(element => {
        trList += `<tr>
      <td>`+ element.id + `</td>
      <td>`+ element.name + `</td>
      <td>`+ element.stock_count + `</td>
    </tr>`;
    });
    return trList;
}
function createMailOptionsFroStockCountTemplate(array) {
    console.log('TRS-create ***********');
    console.log(array);
    console.log('TRS-create ***********');
    return `
   <!DOCTYPE html>
<html>
<head>
<style>
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {
  background-color: #dddddd;
}
</style>
</head>
<body>



<table>
<tr>
    <th>Product ID</th>
    <th>Product</th>
    <th>Current Stock</th>
  </tr>
  `+ createMailOptionsFroStockCountTR(array) + `
</table>

</body>
</html>

   `
}

function createMailOptions(user, type) {
    var htmlTemp = '';
    var subject = '';
    if (type === 0) {
        htmlTemp = createWelcomeTemplateWithPassword(user);
        subject = `Welcome to Motor Web :` + user.name;
    } else if (type === 2) {
        htmlTemp = passwordRestEmail(user)
        subject = `Password Rest`;
    } else if (type === 3) {
        subject = `MotorGarage Online Order Recipt :` + user.products.order_id;
        htmlTemp = invoiceRestEmail(user.products);
    } else {
        htmlTemp = createRegistrationTemplate();
        subject = `Welcome to Motor Web :` + user.name;
    }
    var mailOptions = {
        from: 'madsampath94@gmail.com',//replace with your email
        to: user.email,//replace with your email
        subject: subject,
        html: htmlTemp
    };
    return mailOptions;
}

function sendMail(mailOptions) {
    console.log('in Email sent');
    transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
            console.log(error);
            // res.send('error') // if error occurs send error as response to client
        }
        else {
            console.log('Email sent: ' + info.response);
            // res.send('Sent Successfully')//if mail is sent successfully send Sent successfully as response
        }
    });
}
function invoiceRestEmail(data) {
    return getInvoiceTemplateWithoutRows(data).replace('~##~', createInvoiceTableRows(data.products))
}
/**
 * find from the system
 */
module.exports.find = (condition) => {
    return new Promise((resolve, reject) => {
        orderModel.find(condition).exec((err, order) => {

            err ? reject(err) : resolve(order);
        });
    })
};

module.exports.getAll = (condition) => {
    return new Promise((resolve, reject) => {
        orderModel.find(condition).exec((err, order) => {
            err ? reject(err) : resolve(order);
        });
    })
};
function getAllTotal(data) {
    return new Promise((resolve, reject) => {
        // orderModel.find(condition).exec((err, order) => {
        //     err ? reject(err) : resolve(order);
        // });
        var total = 0;
        if (data && data.length && data.length > 0) {
            data.forEach(element => {
                total += element.total_price
            });
            resolve(total);
        } else {
            resolve(null)
        }

        // orderModel.aggregate([condition,
        //     { $group: { _id: null, sum: { $sum: "$total_price" } } }]).exec((err, order) => {
        //         err ? reject(err) : resolve(order);
        //     });;
    })
};
module.exports.findByIdAndUpdate = async (id, updateObject) => {
    return new Promise(async (resolve, reject) => {
        try {
            orderModel.findOne({ _id: id }, function (err1, orderData) {
                console.log(id);
                console.log(updateObject);
                if (err1) {
                    reject(err1);
                }
                else if (orderData != undefined || orderData != null) {
                    orderModel.update({ _id: id }, { $set: updateObject }).exec((err, data) => {
                        err ? reject(err) : resolve(data);
                    });
                } else {
                    reject("Order ID not found");
                }

            });
        } catch (error) {
            reject("Error while updating order");
        }
    })
};
module.exports.getAllByUser = (condition) => {
    return new Promise((resolve, reject) => {
        let tempOrders = [];
        orderModel.find({}).populate('user_id').exec((err, orders) => {
            // err ? reject(err) : resolve(order);
            if (err) {
                reject(err);
            } else {

                orders.forEach(element => {
                    console.log(element);
                    if (element.user_id && element.user_id._id.toString() === condition.uid.toString()) {
                        console.log("**********************");
                        console.log(orders);
                        console.log("**********************");
                        tempOrders.push(element);
                    }
                });
                resolve(tempOrders);

            }
        });
    });
};
/**
 * remove  from the system
 */
module.exports.delete = (id) => {
    return new Promise((resolve, reject) => {
        orderModel.findOneAndDelete({ _id: id },
            async (err, order) => {
                if (err)
                    reject(err);
                else if (order != null || order != undefined) {
                    resolve("successfully deleted");
                } else
                    reject("Invalid id");
            });
    })
}
function updateProductQuantities(productList) {
    console.log("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
    console.log(productList);
    console.log("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
    var productListEmail = [];
    return new Promise(async (resolve, reject) => {
        for (let index = 0; index < productList.length; index++) {
            let element = productList[index];
            productModel.findOne({ id: element.id }).exec((errNew, productNew) => {
                console.log(productListEmail);
                console.log('productListEmail***************************%%%^^&&&^');
                productModel.update({ id: element.id }, { $inc: { stock_count: ((-1) * parseInt(element.qty)) } }).exec((err, data) => {
                    // productListEmail.push(data);
                    if ((productNew.stock_count - parseInt(element.qty)) <= 3) {
                        productNew.stock_count = (productNew.stock_count - parseInt(element.qty));
                        productListEmail.push(productNew);
                    }
                    console.log(data);
                    if (index <= productList.length - 1) {
                        console.log('last product ############################################# productListEmail before resolve*******');
                        err ? reject(err) : resolve((productListEmail && productListEmail.length > 0) ? productListEmail : false);
                    }
                });
                // err ? reject(err) : resolve(product);
            });
        }
    });
}
/**
 * register new to the system
 */
module.exports.create = (body) => {
    return new Promise(async (resolve, reject) => {
        try {
            userModel.findOne({ id: body.id }, async function (userErr, user) {
                console.log(user);
                console.log(userErr);
                if (userErr) {
                    reject(userErr);
                }
                else if (user != undefined && user != null) {
                    try {
                        const order = new orderModel();
                        var tempId = new Date().getFullYear() + '' + getRandomInt(new Date().getTime());
                        order.products = body.products;
                        order.order_id = tempId.substring(0, tempId.length - 5)
                        order.delivery_date = body.delivery_date;
                        order.created_on_date = body.created_on_date;
                        order.total_price = body.total_price;
                        order.payment_type = body.payment_type;
                        order.order_address = body.order_address;
                        order.recipient_name = body.recipient_name;
                        order.contact_number = body.contact_number;
                        order.delivery_cost = body.delivery_cost;
                        order.status = body.status;
                        order.user_id = body.user_id;
                        order.payment_status = body.payment_type === 'CC' ? 'PAID' : body.payment_type === 'DC' ? 'PAID' : 'NOT COMPLETED';

                        console.log(order);
                        order.save(async (errorOrder, orderData) => {
                            if (errorOrder) {
                                reject(errorOrder);
                            } else {
                                try {
                                    orderModel.findOne({ order_id: orderData.order_id })
                                        .populate('user_id')
                                        .exec(async (errConfirmed, confirmedOrder) => {
                                            console.log("&***********************");
                                            console.log(confirmedOrder);
                                            console.log(errConfirmed);
                                            console.log("&***********************");
                                            if (errConfirmed) {
                                                user_id
                                                reject(errConfirmed);
                                            } else {
                                                opt = createMailOptions({ products: confirmedOrder, email: confirmedOrder.user_id.email }, 3);
                                                sendMail(opt);
                                                console.log("&***********************");
                                                console.log("&***********************");
                                                console.log("&***********************");
                                                console.log("&***********************");
                                                console.log(body.products);
                                                var resEmail = await updateProductQuantities(body.products);
                                                console.log(resEmail);
                                                console.log('resEmail**********');
                                                if (resEmail) {
                                                    var optNew = createMailOptionsFroStockCount({ 'email': 'shehanchamath@gmail.com' }, resEmail);
                                                    sendMail(optNew);
                                                }else{
                                                    console.log("Stock is ok ###########################");
                                                }

                                                resolve(confirmedOrder);
                                            }
                                        })
                                } catch (errorConfirmedOrder) {
                                    reject(errorConfirmedOrder);
                                }

                            }
                        });
                    } catch (errorSave) {
                        reject({ error: true, msg: errorSave });
                    }

                } else {
                    reject("User dosen't exist..!");
                }
            });



        } catch (error) {
            reject('' + error)
        }
    });
}

/**
 * find  from the system by id
 */
module.exports.findOne = (id) => {
    return new Promise((resolve, reject) => {
        orderModel.findOne({ _id: id })
            .exec((err, data) => {
                err ? reject(err) : resolve(data);
            });
    })
};


function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
}


function createInvoiceTableRows(data) {
    var tempRows = '';
    data.forEach(element => {
        tempRows += `<tr class="item">
<td>`+
            element.name
            +
            `</td>
            <td>`+element.qty+`</td>

<td>
   `+
            element.price
            + `
</td>
</tr>`
    });
    return tempRows;

}

function getInvoiceTemplateWithoutRows(order) {
    var ppType = {};
    if (order.payment_type === 'COD') {
        ppType = paymentMethods.COD;
    }
    if (order.payment_type === 'CC') {
        ppType = paymentMethods.CC;
    }
    if (order.payment_type === 'DC') {
        ppType = paymentMethods.DC;
    }

    return `<!doctype html>
    <html>
    <head>
        <meta charset="utf-8">
        <title>A simple, clean, and responsive HTML invoice template</title>
        
        <style>
        .invoice-box {
            max-width: 800px;
            margin: auto;
            padding: 30px;
            border: 1px solid #eee;
            box-shadow: 0 0 10px rgba(0, 0, 0, .15);
            font-size: 16px;
            line-height: 24px;
            font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
            color: #555;
        }
        
        .invoice-box table {
            width: 100%;
            line-height: inherit;
            text-align: left;
        }
        
        .invoice-box table td {
            padding: 5px;
            vertical-align: top;
        }
        
        .invoice-box table tr td:nth-child(2) {
            text-align: right;
        }
        
        .invoice-box table tr.top table td {
            padding-bottom: 20px;
        }
        
        .invoice-box table tr.top table td.title {
            font-size: 45px;
            line-height: 45px;
            color: #333;
        }
        
        .invoice-box table tr.information table td {
            padding-bottom: 40px;
        }
        
        .invoice-box table tr.heading td {
            background: #eee;
            border-bottom: 1px solid #ddd;
            font-weight: bold;
        }
        
        .invoice-box table tr.details td {
            padding-bottom: 20px;
        }
        
        .invoice-box table tr.item td{
            border-bottom: 1px solid #eee;
        }
        
        .invoice-box table tr.item.last td {
            border-bottom: none;
        }
        
        .invoice-box table tr.total td:nth-child(2) {
            border-top: 2px solid #eee;
            font-weight: bold;
        }
        
        @media only screen and (max-width: 600px) {
            .invoice-box table tr.top table td {
                width: 100%;
                display: block;
                text-align: center;
            }
            
            .invoice-box table tr.information table td {
                width: 100%;
                display: block;
                text-align: center;
            }
        }
        
        /** RTL **/
        .rtl {
            direction: rtl;
            font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
        }
        
        .rtl table {
            text-align: right;
        }
        
        .rtl table tr td:nth-child(2) {
            text-align: left;
        }
        </style>
    </head>
    
    <body>
        <div class="invoice-box">
            <table cellpadding="0" cellspacing="0">
                <tr class="top">
                    <td colspan="2">
                        <table>
                            <tr>
                                <td class="title">
                                    <img src="https://cdn.dribbble.com/users/2400067/screenshots/4906539/moto_garage_2-01.png" style="width:100%; max-width:300px;">
                                </td>
                                
                                <td>
                                    Invoice #: `+ order.order_id + `<br>
                                    Created: `+ new Date(order.createdAt).toDateString() + `<br>
                                    Delivery : Within `+ order.delivery_date + `
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                
                <tr class="information">
                    <td colspan="2">
                        <table>
                            <tr>
                                <td>
                                    `+ order.order_address + `
                                </td>
                                
                                <td>
                                `+ order.recipient_name + `<br>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                
                <tr class="heading">
                    <td>
                        Payment Method
                    </td>
                </tr>
                
                <tr class="details">
                    <td>
                    `+ ppType.txt + `
                    </td>
                </tr>
                <tr class="heading">
                    <td>
                        Delivery Charge
                    </td>
                </tr>
                <tr class="details">
                    <td>
                    `+ order.delivery_cost + `<br>
                    </td>
                </tr>
                <tr class="heading">
                    <td>
                        Item
                    </td>
                    <td>
                        Qty
                    </td>
                    
                    <td>
                        Price
                    </td>
                </tr>
                
                ~##~
                <tr class="total">
                    <td></td>
                    <img src="`+ ppType.img + `">
                    
                    <td>
                       <u style='text-decoration: underline double'>Total:`+ order.total_price + ` </u>
                    </td>
                </tr>
            </table>
        </div>
    </body>
    </html>`;
}

/**
 * remove  from the system
 */
module.exports.testQuary = (start, end) => {
    return new Promise((resolve, reject) => {
        orderModel.aggregate(
            [
                {
                    $group:
                    {
                        _id: "$user_id",
                        maxQuantity: { $sum: "$total_price" },
                        maoOrders: { $sum: 1 }
                    }
                },
                { $sort: { maoOrders: -1 } }
            ],
            async (err, product) => {
                if (err)
                    reject(err);
                else if (product != null || product != undefined) {
                    // resolve(product);
                    console.log(product);
                    for (let index = 0; index < product.length; index++) {
                        const element = product[index];
                        var userData = await userService.getUserById(element._id);
                        element['userData'] = userData
                    }
                    resolve(product);
                } else
                    reject(err);
            });
    });
}
module.exports.getOrdersDoneByUsers = async (start, end) => {
    return new Promise((resolve, reject) => {
        console.log("Start date***************" + moment(new Date(new Date(start))).tz("Asia/Colombo").toISOString());
        console.log("End date***************" + moment(new Date(new Date(start))).tz("Asia/Colombo").toISOString());
        orderModel.aggregate(
            [{
                $match: {
                    createdAt: {
                        $gte: new Date(start),
                        $lte: new Date(end)
                    }
                }
            },
            {
                $group:
                {
                    _id: "$user_id",
                    maxQuantity: { $sum: "$total_price" },
                    maoOrders: { $sum: 1 }
                }
            },
            { $sort: { maoOrders: -1 } }
            ],
            async (err, product) => {
                if (err)
                    reject(err);
                else if (product != null || product != undefined) {
                    // resolve(product);
                    console.log(product);
                    for (let index = 0; index < product.length; index++) {
                        const element = product[index];
                        var userData = await userService.getUserById(element._id);
                        element['userData'] = userData
                    }
                    // resolve(product);

                    var htmlBody = createFullPdfTemplateForUserReport(product, start, end);
                    pdf.create(htmlBody, options).toFile('report_order_by_user.pdf', function (err, res) {
                        if (err) {
                            return reject(err)
                        }
                        else {
                            resolve(res);
                        }
                        // console.log(res); // { filename: '/app/businesscard.pdf' }

                    });
                } else
                    reject(err);
            });
    });
}
