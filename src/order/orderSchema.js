const joi = require('joi');

// updateValve registration Schema and validations to be done
module.exports.create = joi.object().keys({
  products: joi.array().required(),
  delivery_date:joi.string().required(),
  created_on_date:joi.number().required(),
  total_price:joi.number().required(),
  payment_type:joi.string().required(),
  order_address:joi.string().required(),
  recipient_name:joi.string().required(),
  contact_number:joi.string().required(),
  delivery_cost:joi.number().required(),
  status:joi.string().required(),
  user_id:joi.string().alphanum().min(24).max(24).required()
});
module.exports.update = joi.object().keys({
  products: joi.array(),
  delivery_date:joi.string(),
  created_on_date:joi.number(),
  total_price:joi.number(),
  payment_type:joi.string(),
  order_address:joi.string(),
  recipient_name:joi.string(),
  contact_number:joi.string(),
  delivery_cost:joi.number(),
  payment_status:joi.string(),
  status:joi.string(),
  user_id:joi.string().alphanum().min(24).max(24)
});