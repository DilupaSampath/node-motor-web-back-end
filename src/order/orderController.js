const orderService = require('./orderService');
const response = require('../../services/responseService');

/**
 * get report
 */
module.exports.createReport = async (req, res) => {
    try {
        console.log('createReport');
        console.log(req.body.status);
        var status= '';
        if(req.body.status){
            status = req.body.status
        }else{
            status = false;
        }
        var pdf = await orderService.createReport(req.body.start,req.body.end,status);
        // pdf.pipe(res);
        // pdf.end();
        response.successWithData(pdf, res)
    } catch (error) {
        response.customError('' + error, res)
    }
}
module.exports.readReportByUser = async (req, res) => {
    try {

        //read the image using fs and send the image content back in the response

        fs.readFile('report_order_by_user.pdf', function (err, content) {
            if (err) {
                res.writeHead(400, { 'Content-type': 'application/pdf' })
                console.log(err);
                res.end("No such pdf");
            } else {
                //specify the content type in the response will be an image
                res.writeHead(200, { 'Content-type': 'application/pdf' });
                res.end(content);
            }
        });
    } catch (error) {
        response.customError('' + error, res)
    }
}
module.exports.readReport = async (req, res) => {
    try {

        //read the image using fs and send the image content back in the response

        fs.readFile('report_order.pdf', function (err, content) {
            if (err) {
                res.writeHead(400, { 'Content-type': 'application/pdf' })
                console.log(err);
                res.end("No such pdf");
            } else {
                //specify the content type in the response will be an image
                res.writeHead(200, { 'Content-type': 'application/pdf' });
                res.end(content);
            }
        });
    } catch (error) {
        response.customError('' + error, res)
    }
}
/**
 * get all 
 */
module.exports.getAll = async (req, res) => {
    try {
        let items = await orderService.getAll({})
        response.successWithData(items, res)
    } catch (error) {
        response.customError('' + error, res)
    }
}
/**
 * test quary 
 */
module.exports.testQuary = async (req, res) => {
    try {
        let items = await orderService.testQuary(req.body.start,req.body.end)
        response.successWithData(items, res)
    } catch (error) {
        response.customError('' + error, res)
    }
}

/**
 * getOrdersDoneByUsers quary 
 */
module.exports.createReportOrdersDoneByUsers = async (req, res) => {
    try {
        let items = await orderService.getOrdersDoneByUsers(req.body.start,req.body.end)
        response.successWithData(items, res)
    } catch (error) {
        response.customError('' + error, res)
    }
}

/**
 * get all 
 */
module.exports.getAllByCondition = async (req, res) => {
    try {
        console.log(req.query);
        let items = await orderService.getAll(req.query)
        response.successWithData(items, res)
    } catch (error) {
        response.customError('' + error, res)
    }
}
/**
 * get all 
 */
module.exports.getAllByUser = async (req, res) => {
    try {
        console.log(req.query);
        let items = await orderService.getAllByUser(req.query)
        response.successWithData(items, res)
    } catch (error) {
        response.customError('' + error, res)
    }
}

/**
 * get one item
 */
module.exports.findOne = async (req, res) => {
    try {
        let item = await orderService.findOne(req.body.id)
        response.successWithData(item, res)
    } catch (error) {
        response.customError('' + error, res)
    }
}

/**
 * create item
 */
module.exports.create = async (req, res) => {
    try {
        let item = await orderService.create(req.body);
        response.successWithData(item, res)
    } catch (error) {
        response.customError('' + error, res)
    }
}

/**
 * update item by id
 * @param {*} req
 * @param {*} res
 */
module.exports.update = async (req, res) => {
    try {
        let details = JSON.parse(JSON.stringify(req.body));
        delete details.id;
        let item = await orderService
            .findByIdAndUpdate(req.params.id, details);
        if (item == null || item == undefined)
            response.customError("Invalid id", res)
        else
            response.successWithData(item, res);
    } catch (error) {
        response.customError('' + error, res);
    }
};

/**
 * remove item by id
 * @param {*} req
 * @param {*} res
 */
module.exports.remove = async (req, res) => {
    try {
        let item = await orderService.delete(req.params.id);
        response.successWithData(item, res);
    } catch (error) {
        response.customError('' + error, res);
    }
};



/**
 * remove item by id
 * @param {*} req
 * @param {*} res
 */
module.exports.validate = async (req, res) => {
    try {
        let item = await orderService.validate(req.body);
        response.successWithData(item, res);
    } catch (error) {
        response.customError('' + error, res);
    }
};