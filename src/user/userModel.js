const crypto = require('crypto');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
// create user Schema
const UserSchema = new Schema(
    {   email: {
            type: String,
            required: true,
        },
        profile_picture: {
            type: String,
            required: false,
        },
        orders: {
            type: Object,
            required: false,
        },
        first_name: {
            type: String,
            required: false,
        },
        last_name: {
            type: String,
            required: false,
        },
        role: {
            type: String,
            required: true,
        },
        active: {
            type: Boolean,
            required: true,
            default: true
        },
        password: {
            type: String,
            required: true,
        },
        salt: String,
    },
    { timestamps: true }
);

// validate password with the database
UserSchema.methods.validPassword = function (password) {
    const hash = crypto
        .pbkdf2Sync(password, this.salt, 10000, 512, 'sha512')
        .toString('hex');
    return this.password === hash;
};
// encrypt the password before save
UserSchema.methods.setPassword = function (password) {
    this.salt = crypto.randomBytes(16).toString('hex');
    this.password = crypto
        .pbkdf2Sync(password, this.salt, 10000, 512, 'sha512')
        .toString('hex');
};

module.exports = mongoose.model('User', UserSchema);
