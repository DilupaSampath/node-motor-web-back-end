const userModel = require('./userModel');
var nodemailer = require('nodemailer');
var smtpTransport = require('nodemailer-smtp-transport');
const config = require('../../config/config');
const crypto = require('crypto');
var pdf = require('html-pdf');
var options = { format: 'Letter' };
var moment = require('moment-timezone');
/**
 * create product report
 */
module.exports.createReport = (start, end) => {
    return new Promise((resolve, reject) => {
        console.log('createReport');
        console.log(moment(new Date(new Date(start))).tz("Asia/Colombo").toISOString());
        userModel.find({
            createdAt: {
                $gte: moment(new Date(new Date(start))).tz("Asia/Colombo").toISOString(),
                $lte: moment(new Date(new Date(end))).tz("Asia/Colombo").toISOString()
            }
        }).exec((err, users) => {
            // err ? reject(err) : resolve(product);

            if (err) {
                reject(err);
            } else {
                if (users) {
                    // add events (here, we draw headers on each new page)

                    // cancel event so the automatic page add is not triggered
                    var dataPdf = findUsersForPdf(users);
                    // ev.cancel = true;
                    var htmlBody = createFullUserTemplate(dataPdf,start,end);
                    pdf.create(htmlBody, options).toFile('report_user.pdf', function (err, res) {
                        if (err) {
                            return reject(err)
                        }
                        else {
                            resolve(res);
                        }
                        // console.log(res); // { filename: '/app/businesscard.pdf' }

                    });
                    //  resolve(pdf);

                    // pdf.addPage();

                    // draw content, by passing data to the addBody method
                }
            }
        });
    });
};
function findUsersForPdf(list) {
    var userList = [];
    list.forEach(element => {
        userList.push({
            email: element.email+ '',
            role: element.role + '',
            createdAt: element.createdAt + '',
            first_name: element.first_name ? element.first_name : '-',
            last_name: element.last_name ? element.last_name : '-',
            active: (element.active ? 'Active' : 'InActive')
        });
    });
    console.log('usrer list pdf');
    console.log(userList);
    return userList;
};
function createTrs(trs) {
    var trList = '';

    trs.forEach(element => {
        trList += `<tr>
      <td>`+ element.email + `</td>
      <td>`+ element.role + `</td>
      <td>`+ element.createdAt.split('GMT')[0]+ `</td>
      <td>`+ element.first_name+ `</td>
      <td>`+ element.last_name + `</td>
      <td>`+ element.active + `</td>
    </tr>`;
    });
    console.log('trs&&&&&&&&&&&&&&');
    console.log(trList);
    console.log('trs&&&&&&&&&&&&&&');
    return trList;
}
function createFullUserTemplate(array,start,end) {
    console.log('TRS-create ***********');
    console.log(array);
    console.log('TRS-create ***********');
    return `
   <!DOCTYPE html>
<html>
<head>
<style>
table {
  font-family: arial, sans-serif;
  font-size: small;
  border-collapse: collapse;
  width: 90%;
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}

</style>
</head>
<body>

<h3>User report for `+start+` to `+end+`</h3>

<table>
<tr>
    <th>User ID</th>
    <th>Role</th>
    <th>Date</th>
    <th>First Name</th>
    <th>Last Name</th>
    <th>Status</th>
  </tr>
  `+ createTrs(array) + `
</table>

</body>
</html>

   `
}
var transporter = nodemailer.createTransport(smtpTransport({
    service: 'gmail',
    auth: {
        user: 'madsampath94@gmail.com',
        pass: 'dsmax071'
    }
}));
/*
/**
* create new user
* @param {*} body 
*/
function createMailOptions(user, type) {
    var htmlTemp = '';
    var subject = '';
    if (type === 0) {
        htmlTemp = createWelcomeTemplateWithPassword(user);
        subject = `Welcome to Motor Web :` + user.name;
    } else if (type === 2) {
        htmlTemp = passwordRestEmail(user)
        subject = `Password Rest`;
    } else if (type === 3) {
        subject = `MotorGarage Online Order Recipt :2321324`;
        htmlTemp = invoiceRestEmail(user.products);
    } else {
        htmlTemp = createRegistrationTemplate();
        subject = `Welcome to Motor Web :` + user.name;
    }
    var mailOptions = {
        from: 'madsampath94@gmail.com',//replace with your email
        to: user.email,//replace with your email
        subject: subject,
        html: htmlTemp
    };
    return mailOptions;
}
function sendMail(mailOptions) {
    console.log('in Email sent');
    transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
            console.log(error);
            // res.send('error') // if error occurs send error as response to client
        }
        else {
            console.log('Email sent: ' + info.response);
            // res.send('Sent Successfully')//if mail is sent successfully send Sent successfully as response
        }
    });
}
module.exports.createUser = (body, role) => {
    console.log("In Service");
    console.log(body);
    console.log("In Service");
    return new Promise((resolve, reject) => {
        var passwordAuto = (Math.random().toString(36)).toString().replace('.', '');
        console.log(passwordAuto);
        userModel.findOne({ email: body.email }, function (err, userData) {
            console.log(userData);
            console.log(err);
            if (err) {
                reject(err);
            }
            else if (userData == undefined || userData == null) {
                const user = new userModel();
                user.email = body.email;
                if (role) {
                    user.role = 'admin';
                } else {
                    user.role = 'user';
                }
                if (body.auto_password) {
                    console.log(body.auto_password);
                    user.setPassword(passwordAuto.toString());
                    if (body.name && body.name.toString().split(' ').length > 1) {
                        user.first_name = body.name.toString().split(' ')[0];
                        user.last_name = body.name.toString().split(' ')[1];
                        console.log(user);
                    } else {
                        user.first_name = body.name;
                    }

                } else {
                    user.setPassword(body.password);
                }
                user.save((error, userSaved) => {
                    if (error) {
                        reject(error);
                    } else {
                        var opt = '';
                        if (body.auto_password) {
                            console.log(body);
                            console.log('body**********************');
                            body.password = passwordAuto;
                            opt = createMailOptions(body, 0);
                            sendMail(opt);
                        } else {
                            opt = createMailOptions(body, 1);
                            sendMail(opt);
                        }
                        resolve({ isFromLogin: false, isAlreadyExist: false, user: userSaved });
                    }
                })
            } else {
                userData['id'] = userData._id;
                userData['status'] = true;
                if (!body.auto_password) {
                    reject("User already exist..!");
                } else {
                    if(userData['active']){
                        resolve({isFromLogin: false, isAlreadyExist: true, user: userData })
                    }else{
                        resolve({foundError : true, error:"Your account is temporarily deactivated", isFromLogin: false, isAlreadyExist: true, user: userData })
                    }
                  
                }
            }
        })
    })
}
module.exports.orderProducts = (products, id) => {
    return new Promise((resolve, reject) => {
        try {
            userModel.findById(id).exec((err, user) => {
                if (err) {
                    reject(err);
                }
                if (user != undefined && user != null) {
                    console.log(user);
                    opt = createMailOptions({ products: products, email: user.email }, 3);
                    sendMail(opt);
                    resolve("Your Online Order Successsfully Placed.");
                }
            });
        } catch (error) {
            reject(error);
        }
    });
};

/**
 * remove  from the system
 */
module.exports.delete = (id) => {
    return new Promise((resolve, reject) => {
        userModel.findByIdAndDelete(id,
            async (err, cart) => {
                if (err)
                    reject(err);
                else if (cart != null || cart != undefined) {
                    resolve("successfully deleted");
                } else
                    reject("Invalid id");
            });
    })
}
module.exports.getAll = (condition) => {
    return new Promise((resolve, reject) => {
        try {
            userModel.find(condition).exec((err, userData) => {
                // err ? reject(err) : resolve(user);
                if (err) {
                    reject(err);
                } else {
                    if (userData == undefined || userData == null) {
                        resolve([]);
                    } else {
                        resolve(userData);
                    }
                }
            });
        } catch (error) {
            reject(error);
        }

    })
};
module.exports.getUserById = (id) => {
    return new Promise((resolve, reject) => {
        userModel.findById(id).exec((err, user) => {
            err ? reject(err) : resolve(user);
        });
    })
};

module.exports.updatetUserById = (id, data) => {
    return new Promise((resolve, reject) => {
        userModel.findByIdAndUpdate(id, { $set: data }).exec((err, user) => {
            err ? reject(err) : resolve(user);
        });
    })
};
module.exports.updatetUserPassword = (id, data) => {
    return new Promise((resolve, reject) => {
        try {
            console.log(user);
            userModel.findById(id, function (err, userData) {
                if (err) {
                    reject(err);
                }
                if (userData != undefined || userData != null) {
                    var userNew = new userModel();
                    userNew.setPassword(data.password.toString());
                    userModel.findByIdAndUpdate({ _id: id }, { $set: userNew }).exec((err, user) => {
                        err ? reject(err) : resolve(user);
                    });
                } else {
                    reject("User not found");
                }
            })
        } catch (error) {
            console.log(error);
            reject('' + error);
        }
    });
};

/**
 * login user 
 * @param {*} usser 
 */
module.exports.resetPassword = (username) => {
    return new Promise((resolve, reject) => {
        try {
            console.log(username);
            userModel.findOne({ email: username }, function (err, userData) {
                if (err) {
                    reject(err);
                }
                if (userData != undefined || userData != null) {
                    console.log(userData);
                    var userMail = {};
                    var passwordAuto = (Math.random().toString(36)).toString().replace('.', '');
                    console.log(passwordAuto);
                    var tempPassword = JSON.parse(JSON.stringify(passwordAuto.toString()));
                    userMail['password'] = tempPassword;
                    userMail['email'] = userData.email;

                    var userUpdate = {};
                    var salt = crypto.randomBytes(16).toString('hex');
                    var passwordScrt = crypto
                        .pbkdf2Sync(tempPassword, salt, 10000, 512, 'sha512')
                        .toString('hex');
                    userUpdate['password'] = passwordScrt;
                    userUpdate['salt'] = salt;

                    userModel.findByIdAndUpdate(userData._id, { $set: userUpdate }).exec((err, user1) => {
                        // err ? reject(err) : resolve(user);
                        if (err) {
                            reject(err)
                        } else {
                            opt = createMailOptions(userMail, 2);
                            sendMail(opt);
                            resolve('Please check your inbox and verify your account');
                        }
                    });


                } else {
                    reject({ inValidEmail: true, msg: "Hey.. there is no account with this user name. Please double check." });
                }
            })
        } catch (error) {
            console.log(error);
            reject('' + error);
        }
    });
}
/**
/**
 * login user 
 * @param {*} usser 
 */
module.exports.loginUser = (user) => {
    return new Promise((resolve, reject) => {
        try {
            console.log(user);
            userModel.findOne({ email: user.email }, function (err, userData) {
                if (err) {
                    reject(err);
                }
                if (userData != undefined || userData != null) {
                    if (userData.active) {
                        let status = userData
                            .validPassword(user.password);
                        if (status) {

                            userData['id'] = userData._id;
                            userData['status'] = true;
                            resolve(userData);
                        } else {
                            reject("Invalid user name or password");
                        }

                    }else{
                        resolve({foundError : true, error:"Your account is temporarily deactivated"});
                    }

                } else {
                    reject("Invalid user name or password");
                }
            })
        } catch (error) {
            console.log(error);
            reject('' + error);
        }
    })
}

// /**
//  * find and update from the system by id
//  */
// module.exports.findByIdAndUpdateScores =async  (id, data) => {
//     return new Promise(async (resolve, reject) => {
//         if(data.score){
//             userModel.findOne({ _id: id })
//             .exec((err, responce) => {
//                if(!err){
//                    console.log(responce);
//                    console.log(responce.score.total);
//                    let newScore =  0;

//                    if(responce.score.total != undefined){
//                     newScore =  responce.score.total + data.score;
//                     if(data.date){

//                     }
//                    }else{
//                     newScore = data.score;
//                    }


//                     userModel.findByIdAndUpdate(id, {score:{total:newScore,date: new Date()}}, { new: false })
//                     .exec((err, result) => {
//                         if (err)
//                             reject(err)
//                         else if (result == null || result == undefined)
//                             reject("Somthing went wrong!");
//                         else
//                             resolve(result);
//                     });
//                }else{
//                 reject("Invalid id");
//                }
//             });

//         }else{
//             reject("user score not found");
//         }

//     })
// };

function createInvoiceTableRows(data) {
    var tempRows = '';
    data.forEach(element => {
        tempRows += `<tr class="item">
<td>`+
            element.name
            +
            `</td>

<td>
   `+
            element.price
            + `
</td>
</tr>`
    });
    return tempRows;

}

function getInvoiceTemplateWithoutRows(id) {
    return `<!doctype html>
    <html>
    <head>
        <meta charset="utf-8">
        <title>A simple, clean, and responsive HTML invoice template</title>
        
        <style>
        .invoice-box {
            max-width: 800px;
            margin: auto;
            padding: 30px;
            border: 1px solid #eee;
            box-shadow: 0 0 10px rgba(0, 0, 0, .15);
            font-size: 16px;
            line-height: 24px;
            font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
            color: #555;
        }
        
        .invoice-box table {
            width: 100%;
            line-height: inherit;
            text-align: left;
        }
        
        .invoice-box table td {
            padding: 5px;
            vertical-align: top;
        }
        
        .invoice-box table tr td:nth-child(2) {
            text-align: right;
        }
        
        .invoice-box table tr.top table td {
            padding-bottom: 20px;
        }
        
        .invoice-box table tr.top table td.title {
            font-size: 45px;
            line-height: 45px;
            color: #333;
        }
        
        .invoice-box table tr.information table td {
            padding-bottom: 40px;
        }
        
        .invoice-box table tr.heading td {
            background: #eee;
            border-bottom: 1px solid #ddd;
            font-weight: bold;
        }
        
        .invoice-box table tr.details td {
            padding-bottom: 20px;
        }
        
        .invoice-box table tr.item td{
            border-bottom: 1px solid #eee;
        }
        
        .invoice-box table tr.item.last td {
            border-bottom: none;
        }
        
        .invoice-box table tr.total td:nth-child(2) {
            border-top: 2px solid #eee;
            font-weight: bold;
        }
        
        @media only screen and (max-width: 600px) {
            .invoice-box table tr.top table td {
                width: 100%;
                display: block;
                text-align: center;
            }
            
            .invoice-box table tr.information table td {
                width: 100%;
                display: block;
                text-align: center;
            }
        }
        
        /** RTL **/
        .rtl {
            direction: rtl;
            font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
        }
        
        .rtl table {
            text-align: right;
        }
        
        .rtl table tr td:nth-child(2) {
            text-align: left;
        }
        </style>
    </head>
    
    <body>
        <div class="invoice-box">
            <table cellpadding="0" cellspacing="0">
                <tr class="top">
                    <td colspan="2">
                        <table>
                            <tr>
                                <td class="title">
                                    <img src="https://www.sparksuite.com/images/logo.png" style="width:100%; max-width:300px;">
                                </td>
                                
                                <td>
                                    Invoice #: `+ id + `<br>
                                    Created: January 1, 2015<br>
                                    Due: February 1, 2015
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                
                <tr class="information">
                    <td colspan="2">
                        <table>
                            <tr>
                                <td>
                                    Sparksuite, Inc.<br>
                                    12345 Sunny Road<br>
                                    Sunnyville, CA 12345
                                </td>
                                
                                <td>
                                    Acme Corp.<br>
                                    John Doe<br>
                                    john@example.com
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                
                <tr class="heading">
                    <td>
                        Payment Method
                    </td>
                    
                    <td>
                        Check #
                    </td>
                </tr>
                
                <tr class="details">
                    <td>
                        Check
                    </td>
                    
                    <td>
                        1000
                    </td>
                </tr>
                
                <tr class="heading">
                    <td>
                        Item
                    </td>
                    
                    <td>
                        Price
                    </td>
                </tr>
                
                ~##~
                <tr class="total">
                    <td>
                    <img src="http://dominionvoice.com/wp-content/uploads/2013/08/Fully-Paid-300x265.jpg">
                    
                    <td>
                       Total: $385.00
                    </td>
                </tr>
            </table>
        </div>
    </body>
    </html>`;
}
function invoiceRestEmail(data) {
    return getInvoiceTemplateWithoutRows(data.order_id).replace('~##~', createInvoiceTableRows(data.all_products))
}
function passwordRestEmail(user) {
    return `
    <!doctype html>
    <html lang="en-US">
    
    <head>
        <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
        <title>Account Creation Email</title>
        <meta name="description" content="Reset Password Email Template.">
        <style type="text/css">
            a:hover {text-decoration: underline !important;}
        </style>
    </head>
    
    <body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px; background-color: #f2f3f8;" leftmargin="0">
        <!--100% body table-->
        <table cellspacing="0" border="0" cellpadding="0" width="100%" bgcolor="#f2f3f8"
            style="@import url(https://fonts.googleapis.com/css?family=Rubik:300,400,500,700|Open+Sans:300,400,600,700); font-family: 'Open Sans', sans-serif;">
            <tr>
                <td>
                    <table style="background-color: #f2f3f8; max-width:670px;  margin:0 auto;" width="100%" border="0"
                        align="center" cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="height:80px;">&nbsp;</td>
                        </tr>
                        <tr>
                            <td style="text-align:center;">
                              <a href="https://rakeshmandal.com" title="logo" target="_blank">
                                <img width="90" src="http://getdrawings.com/free-icon-bw/parts-icon-1.png" title="logo" alt="logo">
                              </a>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:20px;">&nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0"
                                    style="max-width:670px;background:#fff; border-radius:3px; text-align:center;-webkit-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);-moz-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);box-shadow:0 6px 18px 0 rgba(0,0,0,.06);">
                                    <tr>
                                        <td style="height:40px;">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td style="padding:0 35px;">
                                            <h1 style="color:#1e1e2d; font-weight:500; margin:0;font-size:32px;font-family:'Rubik',sans-serif;">Finaly.. Found you...!</h1>
                                            <span
                                                style="display:inline-block; vertical-align:middle; margin:29px 0 26px; border-bottom:1px solid #cecece; width:100px;"></span>
                                            <p style="color:#455056; font-size:15px;line-height:24px; margin:0;">
                                                Hey customer, you're almost ready to start our services.</p>
                                                <p>Please use password <B>${user.password}</B> as yor new login. You can update the password after this verification , click the
                                                following link and follow above instructions.
                                            </p>
                                            <a href="${config.api_url}"
                                                style="background:#20e277;text-decoration:none !important; font-weight:500; margin-top:35px; color:#fff;text-transform:uppercase; font-size:14px;padding:10px 24px;display:inline-block;border-radius:50px;">Motor Web Login</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="height:40px;">&nbsp;</td>
                                    </tr>
                                </table>
                            </td>
                        <tr>
                            <td style="height:20px;">&nbsp;</td>
                        </tr>
                        <tr>
                            <td style="text-align:center;">
                                <p style="font-size:14px; color:rgba(69, 80, 86, 0.7411764705882353); line-height:18px; margin:0 0 0;">&copy; <strong>www.motorweb.lk</strong></p>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:80px;">&nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <!--/100% body table-->
    </body>
    
    </html>`}

function createRegistrationTemplate() {
    return `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  
  <head>
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>
    <style type="text/css">
      @media yahoo{.kmHide,.kmMobileOnly,.kmMobileHeaderStackDesktopNone,.kmMobileWrapHeaderDesktopNone{display:none;font-size:0 !important;line-height:0 !important;mso-hide:all !important;max-height:0 !important;max-width:0 !important;width:0 !important}}@media only screen and (max-width:480px){body,table,td,p,a,li,blockquote{-webkit-text-size-adjust:none !important}body{width:100% !important;min-width:100% !important}#bodyCell{padding:0 !important}table.kmMobileHide{display:none !important}table.kmDesktopOnly,td.kmDesktopOnly,th.kmDesktopOnly,tr.kmDesktopOnly,td.kmDesktopWrapHeaderMobileNone{display:none !important}table.kmMobileOnly{display:table !important}tr.kmMobileOnly{display:table-row !important}td.kmMobileOnly,td.kmDesktopWrapHeader,th.kmMobileOnly{display:table-cell !important}tr.kmMobileNoAlign,table.kmMobileNoAlign{float:none !important;text-align:initial !important;vertical-align:middle !important;table-layout:fixed !important}tr.kmMobileCenterAlign{float:none !important;text-align:center !important;vertical-align:middle !important;table-layout:fixed !important}td.kmButtonCollection{padding-left:9px !important;padding-right:9px !important;padding-top:9px !important;padding-bottom:9px !important}td.kmMobileHeaderStackDesktopNone,img.kmMobileHeaderStackDesktopNone,td.kmMobileHeaderStack{display:block !important;margin-left:auto !important;margin-right:auto !important;padding-bottom:9px !important;padding-right:0 !important;padding-left:0 !important}td.kmMobileWrapHeader,td.kmMobileWrapHeaderDesktopNone{display:inline-block !important}td.kmMobileHeaderSpacing{padding-right:10px !important}td.kmMobileHeaderNoSpacing{padding-right:0 !important}table.kmDesktopAutoWidth{width:inherit !important}table.kmMobileAutoWidth{width:100% !important}table.kmTextContentContainer{width:100% !important}table.kmBoxedTextContentContainer{width:100% !important}td.kmImageContent{padding-left:0 !important;padding-right:0 !important}img.kmImage{width:100% !important}td.kmMobileStretch{padding-left:0 !important;padding-right:0 !important}table.kmSplitContentLeftContentContainer,table.kmSplitContentRightContentContainer,table.kmColumnContainer,td.kmVerticalButtonBarContentOuter table.kmButtonBarContent,td.kmVerticalButtonCollectionContentOuter table.kmButtonCollectionContent,table.kmVerticalButton,table.kmVerticalButtonContent{width:100% !important}td.kmButtonCollectionInner{padding-left:9px !important;padding-right:9px !important;padding-top:9px !important;padding-bottom:9px !important}td.kmVerticalButtonIconContent,td.kmVerticalButtonTextContent,td.kmVerticalButtonContentOuter{padding-left:0 !important;padding-right:0 !important;padding-bottom:9px !important}table.kmSplitContentLeftContentContainer td.kmTextContent,table.kmSplitContentRightContentContainer td.kmTextContent,table.kmColumnContainer td.kmTextContent,table.kmSplitContentLeftContentContainer td.kmImageContent,table.kmSplitContentRightContentContainer td.kmImageContent{padding-top:9px !important}td.rowContainer.kmFloatLeft,td.rowContainer.kmFloatLeft,td.rowContainer.kmFloatLeft.firstColumn,td.rowContainer.kmFloatLeft.firstColumn,td.rowContainer.kmFloatLeft.lastColumn,td.rowContainer.kmFloatLeft.lastColumn{float:left;clear:both;width:100% !important}table.templateContainer,table.templateContainer.brandingContainer,div.templateContainer,div.templateContainer.brandingContainer,table.templateRow{max-width:600px !important;width:100% !important}h1{font-size:40px !important;line-height:1.1 !important}h2{font-size:32px !important;line-height:1.1 !important}h3{font-size:24px !important;line-height:1.1 !important}h4{font-size:18px !important;line-height:1.1 !important}td.kmTextContent{font-size:14px !important;line-height:1.3 !important}td.kmTextBlockInner td.kmTextContent{padding-right:18px !important;padding-left:18px !important}table.kmTableBlock.kmTableMobile td.kmTableBlockInner{padding-left:9px !important;padding-right:9px !important}table.kmTableBlock.kmTableMobile td.kmTableBlockInner .kmTextContent{font-size:14px !important;line-height:1.3 !important;padding-left:4px !important;padding-right:4px !important}}
    </style>
    <!--[if mso]>
      <style>
        .templateContainer {<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">

<!-- START HEADER/BANNER -->

		<tbody><tr>
			<td align="center">
				<table class="col-600" width="600" border="0" align="center" cellpadding="0" cellspacing="0">
					<tbody><tr>
						<td align="center" valign="top" background="https://mcdn.wallpapersafari.com/medium/27/94/ZtgqOC.jpg" bgcolor="#66809b" style="background-size:cover; background-position:top;height=" 400""="">
							<table class="col-600" width="600" height="400" border="0" align="center" cellpadding="0" cellspacing="0">

								<tbody><tr>
									<td height="40"></td>
								</tr>


								<tr>
									<td align="center" style="background: #00000047;
    line-height: 0px;
    color: darkturquoise;
    font-weight: bold;
    font-size: x-large;">
										MoTOR WeB
									</td>
								</tr>



								<tr>
									<td align="center" style="background: #9acd3270;font-family: 'Raleway', sans-serif; font-size:37px; color: #ff15d7; line-height:24px; font-weight: bold; letter-spacing: 7px;">
										OLD is <span style="font-family: 'Raleway', sans-serif; font-size:37px;     color: #f3bd1c; line-height:39px; font-weight: bold; letter-spacing: 7px;">GOALD</span>
									</td>
								</tr>





								<tr>
									<td align="center" style="       background-color: #42de4ba8; font-weight: 600 !important;font-family: 'Lato', sans-serif; font-size:15px; color:#ffffff; line-height:24px; font-weight: 300;">
                                    We are the sri lanka's largest motor cycle spare parts distributors. <br>Enjoy our services with our wonderful solutions.
									</td>
								</tr>


								<tr>
									<td height="50"></td>
								</tr>
							</tbody></table>
						</td>
					</tr>
				</tbody></table>
			</td>
		</tr>


<!-- END HEADER/BANNER -->


<!-- START 3 BOX SHOWCASE -->

		

			<tr>
					<td height="5"></td>
		</tr>


<!-- END 3 BOX SHOWCASE -->


<!-- START AWESOME TITLE -->

		<tr>
			<td align="center">
				<table align="center" class="col-600" width="600" border="0" cellspacing="0" cellpadding="0">
					<tbody><tr>
						<td align="center" bgcolor="#2a3b4c">
							<table class="col-600" width="600" align="center" border="0" cellspacing="0" cellpadding="0">
								<tbody><tr>
									<td height="33"></td>
								</tr>
								<tr>
									<td>


										<table class="col1" width="183" border="0" align="left" cellpadding="0" cellspacing="0">

											<tbody><tr>
											<td height="18"></td>
											</tr>

											<tr>
												<td align="center">
													<img style="display:block; line-height:0px; font-size:0px; border:0px;" class="images_style" src="https://designmodo.com/demo/emailtemplate/images/icon-title.png" alt="img" width="156" height="136">
												</td>



											</tr>
										</tbody></table>



										<table class="col3_one" width="380" border="0" align="right" cellpadding="0" cellspacing="0">

											<tbody><tr align="left" valign="top">
												<td style="font-family: 'Raleway', sans-serif; font-size:20px; color:#f1c40f; line-height:30px; font-weight: bold;">Welcome Home..! You are in now. </td>
											</tr>


											<tr>
												<td height="5"></td>
											</tr>


											<tr align="left" valign="top">
												<td style="font-family: 'Lato', sans-serif; font-size:14px; color:#fff; line-height:24px; font-weight: 300;">
													Your regitration is successfull.Click below link to get start.
												</td>
											</tr>

											<tr>
												<td height="10"></td>
											</tr>

											<tr align="left" valign="top">
												<td>
													<table class="button" style="border: 2px solid #fff;" bgcolor="#2b3c4d" width="30%" border="0" cellpadding="0" cellspacing="0">
														<tbody><tr>
															<td width="10"></td>
															<td height="30" align="center" style="font-family: 'Open Sans', Arial, sans-serif; font-size:13px; color:#ffffff;">
																<a href="${config.api_url}" style="color:#ffffff;">Get Start</a>
															</td>
															<td width="10"></td>
														</tr>
													</tbody></table>
												</td>
											</tr>

										</tbody></table>
									</td>
								</tr>
								<tr>
									<td height="33"></td>
								</tr>
							</tbody></table>
						</td>
					</tr>
				</tbody></table>
			</td>
		</tr>


<!-- END AWESOME TITLE -->


<!-- START WHAT WE DO -->

<!-- END FOOTER -->

						
					
				</tbody></table>`
}

function createWelcomeTemplateWithPassword(user) {
    return `
<!doctype html>
<html lang="en-US">

<head>
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
    <title>Account Creation Email</title>
    <meta name="description" content="Reset Password Email Template.">
    <style type="text/css">
        a:hover {text-decoration: underline !important;}
    </style>
</head>

<body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px; background-color: #f2f3f8;" leftmargin="0">
    <!--100% body table-->
    <table cellspacing="0" border="0" cellpadding="0" width="100%" bgcolor="#f2f3f8"
        style="@import url(https://fonts.googleapis.com/css?family=Rubik:300,400,500,700|Open+Sans:300,400,600,700); font-family: 'Open Sans', sans-serif;">
        <tr>
            <td>
                <table style="background-color: #f2f3f8; max-width:670px;  margin:0 auto;" width="100%" border="0"
                    align="center" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="height:80px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="text-align:center;">
                          <a href="https://rakeshmandal.com" title="logo" target="_blank">
                            <img width="90" src="http://getdrawings.com/free-icon-bw/parts-icon-1.png" title="logo" alt="logo">
                          </a>
                        </td>
                    </tr>
                    <tr>
                        <td style="height:20px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0"
                                style="max-width:670px;background:#fff; border-radius:3px; text-align:center;-webkit-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);-moz-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);box-shadow:0 6px 18px 0 rgba(0,0,0,.06);">
                                <tr>
                                    <td style="height:40px;">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td style="padding:0 35px;">
                                        <h1 style="color:#1e1e2d; font-weight:500; margin:0;font-size:32px;font-family:'Rubik',sans-serif;">Email Confirmation</h1>
                                        <span
                                            style="display:inline-block; vertical-align:middle; margin:29px 0 26px; border-bottom:1px solid #cecece; width:100px;"></span>
                                        <p style="color:#455056; font-size:15px;line-height:24px; margin:0;">
                                            Hey ${user.name}, you're almost ready to start enjoying our services.</p>
                                            <p>Please use password <B>${user.password}</B> as login. You can change the password after this verification , click the
                                            following link and follow above instructions.
                                        </p>
                                        <a href="${config.api_url}"
                                            style="background:#20e277;text-decoration:none !important; font-weight:500; margin-top:35px; color:#fff;text-transform:uppercase; font-size:14px;padding:10px 24px;display:inline-block;border-radius:50px;">Motor Web Login</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height:40px;">&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    <tr>
                        <td style="height:20px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="text-align:center;">
                            <p style="font-size:14px; color:rgba(69, 80, 86, 0.7411764705882353); line-height:18px; margin:0 0 0;">&copy; <strong>www.motorweb.lk</strong></p>
                        </td>
                    </tr>
                    <tr>
                        <td style="height:80px;">&nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <!--/100% body table-->
</body>

</html>`}