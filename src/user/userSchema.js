// import validator class
const joi = require('joi');

// user scorce Schema and validations to be done
module.exports.scores = joi.object().keys({
    score: joi.number().required(),
    id: joi.string().alphanum().min(24).max(24).required(),
});
// user reporting Schema and validations to be done
module.exports.reporting = joi.object().keys({
    free_sheets: joi.boolean().required(),
    is_last_station_on_time: joi.boolean().required(),
    crowd: joi.boolean().required(),
    id: joi.string().alphanum().min(24).max(24).required(),
});
module.exports.login = joi.object().keys({
    email: joi
        .string()
        .email()
        .required(),
    password: joi.required(),
});
// module.exports.order = joi.object().keys({
//     all_products: joi.array().items(),
//     password: joi.required(),
// });
// user registration Schema and validations to be done
module.exports.newUser = joi.object().keys({
    email: joi
        .string()
        .email()
        .required(),
    profile_picture: joi
        .string(),
    auto_password: joi.boolean(),
    orders: joi
        .string(),
    first_name: joi
        .string(),
    active: joi
        .boolean(),
    last_name: joi
        .string(),
    role: joi
        .string(),
    password: joi.string()
        .min(6).when('auto_password', { is: true, then: joi.allow(), otherwise: joi.required() }),
    name: joi.allow()
});

module.exports.updateUser = joi.object().keys({
    email: joi
        .string()
        .email(),
    profile_picture: joi
        .string(),
    orders: joi
        .string(),
    first_name: joi
        .string(),
    last_name: joi
        .string(),
    role: joi
        .string(),
    active: joi
        .boolean(),
    password: joi.string()
        .min(6),
});