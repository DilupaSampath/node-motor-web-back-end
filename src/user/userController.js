const userService = require('./userService');
const response = require('../../services/responseService');
const crypto = require('crypto');

/**
 * get report
 */
module.exports.createReport = async (req, res) => {
    try {
        console.log('createReport');
        console.log(req.body);
        let pdf = await userService.createReport(req.body.start,req.body.end);
        // pdf.pipe(res);
        // pdf.end();
        response.successWithData(pdf, res)
    } catch (error) {
        response.customError('' + error, res)
    }
}
module.exports.readReport = async (req, res) => {
    try {

        //read the image using fs and send the image content back in the response

        fs.readFile('report_user.pdf', function (err, content) {
            if (err) {
                res.writeHead(400, { 'Content-type': 'application/pdf' })
                console.log(err);
                res.end("No such pdf");
            } else {
                //specify the content type in the response will be an image
                res.writeHead(200, { 'Content-type': 'application/pdf' });
                res.end(content);
            }
        });
    } catch (error) {
        response.customError('' + error, res)
    }
}
/**
 * add new user to the system
 * @param {*} req
 * @param {*} res
 */
module.exports.newUser = async (req, res) => {
    try {
        console.log("In contraller");
        console.log(req.body);
        console.log("In contraller");
        let user = await userService.createUser(req.body,null);
        response.successTokenWithData(user, res);
    } catch (error) {
        response.customError('' + error, res);
    }
};
/**
 * add new user to the system
 * @param {*} req
 * @param {*} res
 */
module.exports.newUserAdmin = async (req, res) => {
    try {
        console.log("In contraller");
        console.log(req.body);
        console.log("In contraller");
        let user = await userService.createUser(req.body,'admin');
        response.successTokenWithData(user, res);
    } catch (error) {
        response.customError('' + error, res);
    }
};
/**
 * add new user to the system
 * @param {*} req
 * @param {*} res
 */
module.exports.orderProducts = async (req, res) => {
    try {
        console.log("In contraller");
        console.log(req.body);
        console.log(req.params.id);
        console.log("In contraller");
        let data = await userService.orderProducts(req.body,req.params.id);
        response.successTokenWithData(data, res);
    } catch (error) {
        response.customError('' + error, res);
    }
};
/**
 * remove item by id
 * @param {*} req
 * @param {*} res
 */
module.exports.remove = async (req, res) => {
    try {
        let item = await userService.delete(req.params.id);
        response.successWithData(item, res);
    } catch (error) {
        response.customError('' + error, res);
    }
};
/**
 * get all 
 */
module.exports.getAll = async (req, res) => {
    try {
        let items = await userService.getAll({})
        response.successWithData(items, res)
    } catch (error) {
        response.customError('' + error, res)
    }
}
/**
 * get all 
 */
module.exports.resetPassword = async (req, res) => {
    try {
        console.log(req.params.username);
        let responce = await userService.resetPassword(req.params.username)
        response.successWithData(responce, res)
    } catch (error) {
        response.customError(error, res)
    }
}
/**
 * remove user 
 */
module.exports.getUser = async (req, res) => {
    try {
        console.log(req.params.id);
        let items = await userService.getUserById(req.params.id)
        response.successWithData(items, res)
    } catch (error) {
        response.customError('' + error, res)
    }
}
/**
 * get all 
 */
module.exports.getUser = async (req, res) => {
    try {
        console.log(req.params.id);
        let items = await userService.getUserById(req.params.id)
        response.successWithData(items, res)
    } catch (error) {
        response.customError('' + error, res)
    }
}
/**
 * user login to the system
 * @param {*} req
 * @param {*} res
 */
module.exports.login = async (req, res) => {
    try {
        let user = await userService.loginUser(req.body);
        response.successTokenWithData(user, res);
    } catch (error) {
        response.customError('' + error, res);
    }
};
/**
 * user update existing user in the system
 * @param {*} req
 * @param {*} res
 */
module.exports.updateUser = async (req, res) => {
    try {
        console.log(req.params.id);
        var details = JSON.parse(JSON.stringify(req.body));
        delete details.id;
        delete details._id;
        delete details.uid;
        if (details.password) {
            var salt = crypto.randomBytes(16).toString('hex');
            var passwordScrt = crypto
                .pbkdf2Sync(details.password, salt, 10000, 512, 'sha512')
                .toString('hex');
            details['salt'] = salt;
            details['password'] = passwordScrt;
        }

        console.log(details);
        let user = await userService.updatetUserById(req.params.id, details);
        response.successTokenWithData(user, res);
    } catch (error) {
        response.customError('' + error, res);
    }
};
// /**
//  * update item by id
//  * @param {*} req
//  * @param {*} res
//  */
// module.exports.updateScores = async (req, res) => {
//     try {
//         let details = JSON.parse(JSON.stringify(req.body));
//         let item = await userService
//             .findByIdAndUpdateScores(req.body.id, details);
//         if (item == null || item == undefined)
//             response.customError("Invalid id", res)
//         else
//             response.successWithData(item, res);
//     } catch (error) {
//         response.customError('' + error, res);
//     }
// };